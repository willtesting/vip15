# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/6 15:35
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
from class6.homework import my_time_new


class MySort:

    @my_time_new(t='ms')
    def bubble_sort(self,height):
        # 控制比较的轮次
        # 为什么减1？因为剩下最后一个元素的时候，根本就不用比了
        for j in range(len(height) - 1):

            # 为什么-j？因为每一轮比较之后，最后一个元素都不参与下一轮比较了

            # 冒泡排序第一轮：遍历列表，两两比较如果顺序错误就交换
            # 为什么减1？因为两两比较，4个元素只需要比3次
            for i in range(len(height) - 1 - j):
                # 如果顺序错误就交换
                if height[i] > height[i + 1]:
                    height[i], height[i + 1] = height[i + 1], height[i]

    @my_time_new()
    def select_sort(self,height):
        # 控制比较的轮次
        # 为什么减1？因为剩下最后一个元素的时候，根本就不用比了
        for j in range(len(height) - 1):

            # 这里一定要-j？每一轮比较，最后一个元素都不用参与，否则一定选出来是上一轮最大的

            # 第一轮，选出最大的，放到列表末尾
            # 记录最大的下标（你选取了第一个元素作为最大）
            max_index = 0
            for i in range(1, len(height) - j):
                # 每次遍历都和最大的比较，如果元素比当前最大的大，就把最大的下标变成当前元素
                if height[i] > height[max_index]:
                    max_index = i

            # 因为每一轮比较，剩下的最后一个都往前推一个
            # 选出最大的后，把它交换到末尾
            height[max_index], height[len(height) - 1 - j] = height[len(height) - 1 - j], height[max_index]

    def quick_sort(self,list1, left, right):
        """
        快排
        :param list1: 待排序列表
        :param left: 左边界
        :param right: 右边界
        :return:
        """

        # 选取待排序元素最右边为基准
        base = list1[right]

        # 初始化游标的位置
        l, h = left, right

        while l < h:
            while l < h:
                # 从左往右找比基准大的
                if list1[l] > base:
                    # 如果找到了
                    # 交换到h位置
                    list1[l], list1[h] = list1[h], list1[l]
                    # h 往左移动一位
                    h -= 1
                    # 交换位置
                    break
                else:
                    # 如果没找到呢？l往右移动一位继续找
                    l += 1

            while l < h:
                if list1[h] < base:
                    # 交换到l位置
                    list1[l], list1[h] = list1[h], list1[l]
                    l += 1
                    # 交换位置
                    break
                else:
                    h -= 1

        # 左边递归（出口是什么？）
        if l <= left + 1:
            # 当左边小于等于一个元素的时候，不用递归了
            pass
        else:
            self.quick_sort(list1, left, l - 1)

        if h >= right - 1:
            pass
        else:
            self.quick_sort(list1, h + 1, right)


class HuMan:
    # 类变量，表示这个类的对象都是人类
    t = '人类'

    def __init__(self, name='will'):
        # 实例变量，表示每个人的年龄，身高，姓名等
        self.age = 0
        self.height = 50
        self.name = name

    def grow_up(self, h=10):
        """
        长大，每次长一岁，身高不定
        :param h: 长高了多少
        :return:
        """
        self.age += 1
        self.height += h

    def say(self):
        print('hello')

    @classmethod
    def get_type(cls):
        return cls.t


class Man(HuMan):
    # 拓展
    gender = 'male'

    def __init__(self, name='will'):
        # 继承，调父类构造函数
        super().__init__(name)
        # 拓展
        self.hobby = '美女'

    def say(self):
        # 重写：同时继承实例变量
        print(f'嘿，你好，我是{self.name}')

    def love(self):
        # 拓展
        print(f'美女你好，我是{self.name}，我的爱好是{self.hobby}')

    @classmethod
    def get_gender(cls):
        return cls.gender


if __name__ == '__main__':
    # # 把这个模块作为主运行入口（右键run这样）执行的时候，才会运行
    # # import导入是不执行这里的
    # my_sort = MySort()
    # list1 = [i for i in range(1999, 0, -1)]
    # my_sort.bubble_sort(list1)
    #
    # list1 = [i for i in range(1999, 0, -1)]
    # my_sort.select_sort(list1)

    human = HuMan('will')
    human.grow_up()
    print(human.height)
