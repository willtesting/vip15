# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/4 20:15
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：模块化封装
"""
from class6.homework import select_sort
from class7.demo2 import HuMan
from class7 import demo4

h = [1, 3, 2, 5, 8, 9, 4, 7]
# h.sort()
# print(h)
select_sort(h)
print(h)

will = HuMan()
HuMan.get_type()

# 全局变量，可以通过模块导入使用
print(demo4.token)