# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/4 21:03
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：全局变量和局部变量
"""
# 全局变量：定义在整个库中，对整个库起作用的变量，用global引用
token = '这是token'

# 局部变量
def my_token():
    # 但这是一个局部变量
    # 定义在某一个缩进当中，只对当前缩进过程和子过程生效的变量
    token = 1
    # if token:
    #     x = 1
    #     print(x)
    # else:
    #     print(x)
    print(token)


def my_token2():
    global token
    # 这里使用了全局变量
    token = 2
    print(token)



my_token()
my_token2()
print(token)

