# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/4 20:29
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：人类
"""


# 说话
# 会打篮球
# 化妆
# 买化妆品
# 跳舞

class HuMan:
    # 类变量：描述整个类共有的属性
    t = "人类"

    # 类里面的所有函数，除了第一个参数必须是self，或者cls，其他的都和普通函数一模一样
    def __init__(self, ktc='hi'):
        """
        __init__构造函数：是用来创建对象的
        如果你没有定义构造函数，那么系统会提供一个默认的空构造函数
        在构造函数中，使用self定义的变量，我们称为实例变量
        """
        print('创建对象')
        # 实例变量：描述每一个实例特有的属性
        self.koutouchan = ktc
        self.name = ''
        self.hua_zhuang_pin = ['眼线笔']
        self.__d = '开心'

    def say(self, s):
        """说话"""
        print(f"{self.name} say: {self.koutouchan},{s}")

    def play(self):
        self.name = 'will'
        print(f'{self.name}打篮球')

    @classmethod
    def get_type(cls):
        """类函数"""
        print(cls.t)

    def make_up(self, *args):
        """化妆"""
        self.hua_zhuang_pin = list(args)
        print('开始化妆')
        for h in self.hua_zhuang_pin:
            print(f'使用：{h}')

        print('化妆完成')
        self.__dancy()

    def __dancy(self):
        """
        私有函数，只能在类里面使用的
        :return:
        """
        if self.__d == '开心':
            print('跳舞')


if __name__ == '__main__':
    # 创建一个实例对象：其实是调用了__init__函数
    will = HuMan()
    # 类函数一般使用类调用
    HuMan.t = '机器人'
    HuMan.get_type()
    roy = HuMan('hello')

    will.say('同学们')
    roy.say('同学们')

    roy.make_up(*['口红', '粉底', '水乳'])
    # 类外面不能用私有属性和方法的
    # print(roy.__d)
    # roy.__dancy()
