# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/4 20:18
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：面向对象
"""
# 字符串数据类型，不仅存了数据，还提供了方法
s = '1 2 3 4 5 6'
print(s)
s = s.split(' ')
print(s)


class HuMan:
    # 类型描述
    t = '人类'

    @classmethod
    def get_type(cls):
        print(cls.t)
