# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/4 21:31
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：继承
"""
from class7.demo3 import HuMan


class WoMan(HuMan):
    """继承：这里的文字，可以用__doc__基础属性获取"""

    def __init__(self, ktc='hi'):
        """
        如果子类定义了构造函数，那么在构造函数里面一定要调用父类的构造函数
        """
        # 调父类构造函数（如果父类构造函数需要参数，你可以选择原样定义参数，然后传进去）
        # 只适合单继承
        # super().__init__(ktc)
        # 多继承必须用这种
        HuMan.__init__(self, ktc)
        self.hua_zhuang_pin = ['眼线笔', '睫毛膏']
        # 余额（拓展）
        self.money = 30

    def make_up(self, l: list):
        self.hua_zhuang_pin += l
        print('开始化妆')
        for h in self.hua_zhuang_pin:
            print(f'使用：{h}')

        print('化妆完成')

    def buy(self, l: list):
        """
        拓展：写父类没有功能
        :return:
        """
        for ll in l:
            if self.money > 0:
                self.hua_zhuang_pin.append(ll)
                self.money -= 10
            else:
                print('余额不足')


if __name__ == '__main__':
    youmi = WoMan('hi~')
    youmi.say('你好')
    # 买化妆品（拓展）
    youmi.buy(['a', 'b', 'c', 'd'])
    youmi.make_up(['口红'])
    # # 私有的东西不能被继承
    # youmi.__dancy()
    print(youmi.__doc__)
