# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/30 21:54
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：快排
"""

# list1 = [4, 1, 9, 7, 2, 5, 6, 8, 10, 3]
list1 = [i for i in range(999,0,-1)]


def quick_sort(list1, left, right):
    """
    快排
    :param list1: 待排序列表
    :param left: 左边界
    :param right: 右边界
    :return:
    """

    # 选取待排序元素最右边为基准
    base = list1[right]

    # 初始化游标的位置
    l, h = left, right

    while l < h:
        while l < h:
            # 从左往右找比基准大的
            if list1[l] > base:
                # 如果找到了
                # 交换到h位置
                list1[l], list1[h] = list1[h], list1[l]
                # h 往左移动一位
                h -= 1
                # 交换位置
                break
            else:
                # 如果没找到呢？l往右移动一位继续找
                l += 1

        while l < h:
            if list1[h] < base:
                # 交换到l位置
                list1[l], list1[h] = list1[h], list1[l]
                l += 1
                # 交换位置
                break
            else:
                h -= 1

    # 左边递归（出口是什么？）
    if l <= left + 1:
        # 当左边小于等于一个元素的时候，不用递归了
        pass
    else:
        quick_sort(list1, left, l - 1)

    if h >= right - 1:
        pass
    else:
        quick_sort(list1, h + 1, right)


quick_sort(list1, 0, len(list1) - 1)
print(list1)
