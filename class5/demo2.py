# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/30 20:19
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：函数
"""
# # 系统函数
# s = min([1, 2, 3, 4])
# print(s)


# 实现1+1=2的函数
def my_add(x, y):
    """
    实现x+y=z
    :param x: 加数1
    :param y: 加数2
    :return: 返回和
    """
    print('执行了')
    # 函数体
    z = x + y

    # 返回函数处理后的结果
    return z


# print(my_add.__doc__)
# 执行函数，可以把执行结果存到变量里面
s = my_add(1,1)
print(s)