# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/30 21:33
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：递归（函数在运行过程中调用自己）
"""


# 阶乘定义：n! = n * (n-1)!， 1！ = 1。
def facs(n):
    # 出口
    if n == 1:
        return 1

    return n * facs(n - 1)


print(facs(4))
