# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/1 20:01
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：作业
"""
import time



def my_huiwen(s: str):
    """
    写一个模块，完成以下功能：
    函数功能：
    给定一个字符串（回文字符串）（参数）
    先判断是否刚好是对称的两部分（怎么判断字符串是否是对称的?）
    :param s: 目标字符串
    :return: 如果是，返回字符串相反的两部分，不是返回False
    """
    # 如果s是空字符串
    if not s:
        return False

    # 如果是对称的两部分，那么s和它反转后的字符串是一样
    if s == s[::-1]:
        # 返回相反的两部分，需要考虑是奇数还是偶数长度
        l = len(s)
        if l % 2 == 1:
            return s[:l // 2 + 1], s[l // 2:]
        else:
            return s[:l // 2], s[l // 2:]
    else:
        return False


print(my_huiwen('abba'))

# height = [4, 1, 9, 7, 2, 5, 6, 8, 10, 3]
height = [i for i in range(100,0,-1)]


def quick_sort(height, left, right):
    """
    快排
    :param height: 待排序列表
    :param left: 子列表最左边界
    :param right: 子列表最右边界
    """
    # 排序用的游标
    l, h = left, right
    # 选择基准
    base = height[h]

    # 只要l < h的时候，就重复这样找，直到l=h的时候停止
    while l < h:
        # 从左往右找比基准大，交换到h位置，h往右移动一位
        while l < h:
            # 找到了
            if height[l] > base:
                height[l], height[h] = height[h], height[l]
                h -= 1
                # 如果找到了，接下要交换方向找，所以要break
                break
            else:
                # 往右移动一位继续找
                l += 1

        # 再从h位置从右往左找比基准小的，交换到之前l的位置，l往右移动一位
        while l < h:
            # 找到了
            if height[h] < base:
                height[l], height[h] = height[h], height[l]
                l += 1
                # 如果找到了，接下要交换方向找，所以要break
                break
            else:
                # h往左移动一位继续找
                h -= 1

    # 递归，对左边和右边分别进行快排
    if l <= left + 1:
        # 左边小于等于一个元素的时候，不用排
        pass
    else:
        quick_sort(height, left, l - 1)

    if h >= right - 1:
        # 右边小于等于一个元素的时候，不用排
        pass
    else:
        quick_sort(height, h + 1, right)


quick_sort(height, 0, len(height) - 1)
print(height)
