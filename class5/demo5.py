# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/30 21:17
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：函数的传值与传地址
"""

# 和这两个参数没有关系
a, b = 1, 2

x, y = 3, 4


def func1(a, b):
    """
    形参与实参
    函数调用的时候，把实参的值传递给形参
    :param a: 形参（形式上接收参数的变量）
    :param b: 形参
    :return:
    """
    print(a, b)


# x，y：实参（实际传递的参数）
func1(x, y)


# 传值：实参仅是把值传递形参，然后他们就没有任何关系了
def func2(a1):
    a1 = 2
    print(a1)


a = 1
func2(a)
print(a)


# 传地址：实参和形参同时指向容器变量，改变容器里面内容，自然都会改变
def myfunc3(list_a1):
    print('函数')
    list_a1[0] = 2
    print(list_a1)


list_a = [0,1,2,3]
myfunc3(list_a)
print(list_a)

# 浅拷贝
a = [1,3,4]
b = a
a[1] = 33
print(a,b)

# 深拷贝
a = [1,3,4]
# 重新创建列表，所以是一个新的容器
# b = a[::]
b = a.copy()
a[1] = 33
print(a,b)



