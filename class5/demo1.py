# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/30 20:10
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字符串转义
"""

# python可以选择定义字符串的时候是用单引号还是双引号
s = 'abc"sdlkjfdsa'
print(s)

# 转义（\）：特殊字符转普通字符
s = "dskjf\"dsk"
print(s)
print("dsjfds\\a")

# 转义（\）：普通字符转特殊含义
s = "sdfjs\tdalkf"
print(s)
print('\u2665')

# 忽略字符串里面的转义写法
s = r"sdfjs\tdalkf"
print(s)
file_path = r"D:\vspro\mokeytest"
print(file_path)


