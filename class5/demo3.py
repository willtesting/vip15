# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/30 20:27
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：函数的参数
"""


# 位置型参数
def my_add(x, y, *args):
    """
    实现2~n个数字相加
    :param x: 位置型参数，必填
    :param y: 位置型参数，必填
    :param *args：不定数目的位置型参数，我们接收为一个元组（可以不传，接收到空元组）
    使用*args主要是接收函数没有显示定义的位置型参数
    :return:
    """
    z = x + y
    for arg in args:
        z += arg
    return z


s = my_add(1, 2, 3, 4, 5)
print(s)


# 键值对参数
def say_user_info(name='will', age=18, **kwargs):
    """
    :param name: 键值对参数（相当于给了参数默认值，不传的时候，使用默认值）
    :param age:
    :param **kwargs：接收未显示定义的键值对参数（接收为一个字典）
    按键值对应传递，与顺序无关
    如果你传递的时候，没有用键值对，也会按顺序传(未显示定义的不能按顺序传)
    :return:
    """
    print(kwargs)
    print(name, age, end=' ')
    for key in kwargs.keys():
        print(kwargs.get(key), end='')


say_user_info('roy', 12, sex='male')
print()


# 键值对参数，一定要写在位置型参数的后面
def anylisys(a, *args, b=1, **kwargs):
    """
    键值对参数，一定要写在位置型参数的后面
    :return:
    """
    print(a, b, args, kwargs)


anylisys(1, 2, 3, b=1, c=2)


p1 = [1,2,3,4,5,6]
# 迭代传法my_add(1,2,3,4,5,6)
s = my_add(*p1)
print(s)

# 迭代传：say_user_info(name="tufei",sex="m")
d1 = {'name':"tufei",'sex':"m"}
say_user_info(**d1)

print()
# python37后的拓展写法
# 可以支持参数指定类型（推荐使用类型，会提示，不会报错）
def my_add2(x:int,y:int = 1):
    """
    指定参数类型
    """
    print(x,y)


my_add2('a',3)