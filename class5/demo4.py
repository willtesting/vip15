# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/30 21:01
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：函数返回值
"""


def my_add(x, y):
    """
    参数是传入的数据：函数实现功能需要的数据
    返回值是返回的结果：函数处理后的结果
    :param x:
    :param y:
    :return:
    """
    # 处理特殊情况，函数直接运行完成
    if x < 0:
        # 直接return，没有结果（或者函数没有写return），默认返回值是None
        return

    z = x + y
    # return代表函数执行完成
    # 函数可以返回多个结果，以元组的形式返回
    return z, 1, '2', {'a': 3}
    print(1111)


print(my_add(1, 2))

# 函数封装实战，实现下面的算法
"""
一个字符串替换第n子串1位子串2
n = 3
s = 'will,roy,tufei,roy,will,tufeiroywilltufeiroywill'
s = s.replace('will', '$roy', n)
# 再把前n-1替换回去
s = s.replace('$roy', 'will', n - 1)
s = s.replace('$roy', 'roy')
print(s)
"""


# 参数分析：一个字符串（传进去），第n个（传进去），子串1，子串2
def my_replace(s, old, new, n=1):
    """
    :param s: 目标字符串
    :param old: 需要替换的子串
    :param new: 替换为什么子串
    :param n: 替换的第n个
    :return: 替换后的字符串
    """
    if n < 1:
        return s

    # s = 'will,roy,tufei,roy,will,tufeiroywilltufeiroywill'
    s = s.replace(old, '$'+new, n)
    # 再把前n-1替换回去
    s = s.replace('$'+new, old, n - 1)
    # 然后把标志替换为目标子串
    s = s.replace('$'+new, new)
    return s


s = 'will,roy,tufei,roy,will,tufeiroywilltufeiroywill'
s = my_replace(s,'will','roy',3)
print(s)