# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/28 21:39
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：选择排序
"""
from class6.decs import my_time

# height = [155, 187, 172, 160, 163, 166, 173, 182, 165, 159]
from class6.demo2 import my_time_new

height = [i for i in range(1000,0,-1)]


# 快速导入：alt+enter键导入
@my_time
@my_time_new(t='ss')
def select_sort(height):
    print('执行了select_sort')
    # 控制比较的轮次
    # 为什么减1？因为剩下最后一个元素的时候，根本就不用比了
    for j in range(len(height) - 1):

        # 这里一定要-j？每一轮比较，最后一个元素都不用参与，否则一定选出来是上一轮最大的

        # 第一轮，选出最大的，放到列表末尾
        # 记录最大的下标（你选取了第一个元素作为最大）
        max_index = 0
        for i in range(1, len(height) - j):
            # 每次遍历都和最大的比较，如果元素比当前最大的大，就把最大的下标变成当前元素
            if height[i] > height[max_index]:
                max_index = i

        # 因为每一轮比较，剩下的最后一个都往前推一个
        # 选出最大的后，把它交换到末尾
        height[max_index], height[len(height) - 1 - j] = height[len(height) - 1 - j], height[max_index]


select_sort(height)
# print(height)