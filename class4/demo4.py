# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/28 21:22
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：冒泡排序
"""
# # 系统排序
# # height = [177, 168, 159, 189, 191]
# # height.sort(reverse=False)
# # print(height)
#
from class6.demo1 import my_time

# height = [155, 187, 172, 160, 163, 166, 173, 182, 165, 159]
height = [i for i in range(1000,0,-1)]


# # 怎么height列表里面找出最高的
# m = 0
# # 遍历列表
# for h in height:
#     # 每一次都记录最大的
#     if h > m:
#         m = h
# print(m)
#
# # Python里面变量值交换
# a = 1
# b = 2
# # 重新赋值：a,b = 2,1
# a, b = b, a
# print(a, b)


@my_time(t="μs")
def bubble_sort(height):
    # 控制比较的轮次
    # 为什么减1？因为剩下最后一个元素的时候，根本就不用比了
    for j in range(len(height) - 1):

        # 为什么-j？因为每一轮比较之后，最后一个元素都不参与下一轮比较了

        # 冒泡排序第一轮：遍历列表，两两比较如果顺序错误就交换
        # 为什么减1？因为两两比较，4个元素只需要比3次
        for i in range(len(height) - 1 - j):
            # 如果顺序错误就交换
            if height[i] > height[i + 1]:
                height[i], height[i + 1] = height[i + 1], height[i]


bubble_sort(height)
# print(height)
