# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/28 20:43
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：python正则表达式
"""
import json

s = '''
<html><head><meta http-equiv="Content-Type" content="text/html;charset=utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta content="always" name="referrer"><meta name="theme-color" content="#ffffff"><meta name="description" content="全球领先的中文搜索引擎、致力于让网民更便捷地获取信息，找到所求。百度超过千亿的中文网页数据库，可以瞬间找到相关的搜索结果。"><link rel="shortcut icon" href="
	/favicon.ico
	" type="image/x-icon" /><link rel="search" type="application/opensearchdescription+xml" href="/content-search.xml
	" title="百度搜索" /><link rel="icon" sizes="any" maskhref="//www.baidu.com/img/baidu_85beaf5496f291521eb75ba38eacbd87.svg
	"><link rel="dns-prefetch" href="//dss0.bdstatic.com
	"/><link rel="dns-prefetch" href="//dss1.bdstatic.com
	"/><link rel="dns-prefetch" href="//ss1.bdstatic.com
	"/><link rel="dns-prefetch" href="//sp0.baidu.com
	"/><link rel="dns-prefetch" href="//sp1.baidu.com
	"/><link rel="dns-prefetch" href="//sp2.baidu.com
	"/><title>百度一下，你就知道</title><style index="newi" type="text/css">
	<body>
	<div class="c-tips-container" id="c-tips-container"></div>
	<script>
	var s_session={
		"logId":"3614294667",
		"seqId":"3614294907",
		"sessionId":"",
		"debug":false,
		"userTips":"{}",
		"curmod":"2",
		"firstmod":"",
		"logoCode":false,
		"isFesLogo":false,
		"sid":"36454_34813_36422_36166_36488_36055_36419_26350_36299_36468_36315_36447",
		"mSid":"",
		"sample_value":"",
		"isLogin":false,
		"agingVoice": "",
		"卡卡": "632232258",
		"Will": "1052949192",
		"Phone": "18874922908"
	};
	window.__async_strategy = 2;
	</script>
	</body>
	</html>
'''


import re

# 正则如果匹配到了，返回一个列表，如果匹配不到返回一个空列表
#
# # 场景1：匹配两个字符串中间的字符
# res = re.findall('<title>.*</title>',s)
# print(res)
# # 场景1：匹配两个字符串中间的字符
# res = re.findall('<title>(.*)</title>',s)
# print(res)
#
# # 场景2：提取两个字符串中间的字符，且包含换行
# res = re.findall('var s_session=({.*});',s,re.S)
# print(json.loads(res[0]))

# # 场景5：提取连续数字
# res = re.findall(r'\d{7,}',s)
# print(res)

# # 提取QQ号：第一位不是0，是连续5~14位数字
# res = re.findall(r'[1-9]\d{4,14}',s)
# print(res)
#
# # 提取手机号：第一位一定是1，第二位大于3，连续11位数字
# res = re.findall(r'1[3-9][0-9]{9}',s)
# print(res)

# 获取link标签的内容
# 正常的.*是贪心写法，会尽力匹配最多的内容
res = re.findall('<link.*/>',s,re.S)
print(res)
# 非贪心写法，尽可能匹配最少的内容，如果有多组满足条件内容，会都匹配出来，放到一个列表
res = re.findall('<link.*?/>',s,re.S)
print(res)

# 作业
"""
从下面的订单信息：提取订单号，和价格
订单号：  202207192221383736    |     付款金额（元）：  456.00 元
"""

