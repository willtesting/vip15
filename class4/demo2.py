# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/28 20:24
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字符串替换
"""
s = 'will,roy,tuifei,kakawill,roy,tuifei,kakawill,roy,tuifei,kaka'
# 把所有的kaka替换为youmi
"""
old: 需要被替换的子串
new: 替换的内容
count: 替换的数目（负数代表全部替换，其他的代表最多替换多少次）
把旧子串替换为新子串，并得一个新字符串（必须要重新赋值，才能得到）
"""
ss = s.replace('kaka', 'youmi', 4)
print(ss)

# 替换最后一个子串
"""
1. 把字符串可以看作一个字符列表
    列表是可以反转的，我们先把列表反转，这样最后子串就变成第一个了
2. 把反转后的第一个子串替换
    替换的old和new也都是反的
3. 再替换后的字符串反转回来就好了
"""
sss = s[::-1]
sss = sss.replace('kaka'[::-1], 'youmi'[::-1], 1)
sss = sss[::-1]
print(sss)

# 怎么替换第n个子串？


s = 'aaaa{bbb}ccc'
# 字符串截取
# 在字符串中，从左往右查找第一个大括号出现的下标
start = s.find('{')
print(start)
# 在字符串中，从右往左找第一个出现的下标
end = s.rfind('}')
print(end)

# 截取标志字符串之间子串
ss = s[s.find('{') + 1:s.rfind('}')]
print(ss)
