# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/28 20:06
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字符串切割与拼接
"""
# # 字符串切割
# s = 'will, roy, tufei, kaka'
# p = s.split(', ')
# print(p)
#
# # 拼接（列表元素按节点拼接起来，成为一个字符串）
# ss = ', '.join(p)
# print(ss)

# 需要这种参数转成字典
params = 'username=Will&password=111111&111841312984'

"""
算法描述：
1.键值对之间是用&连接
    切割：先切割得到一个存放键值对的列表
2.键和值之间是用=连接
    切割（处理没有值的情况）：得到键和值
"""

# 先定义字典存参数
dict_params = {}

key_values = params.split('&')
for key_value in key_values:
    # 对每个键值对进行第二步处理
    key_value = key_value.split('=')
    # print(key_value)
    if len(key_value) == 1:
        # 没有值的情况，把切割出来的键作为字典的键，值用None
        dict_params[key_value[0]] = None
    else:
        # 有键值对，第一个元素是键，第二个元素是值
        dict_params[key_value[0]] = key_value[1]

print(dict_params)

# 字典转成form表单参数字符串
"""
1. 键值对使用=拼接
    可以用join也可以用+，没有键的情况（不拼）
2. 键值对之间用&拼接
    推荐用join
"""

# 存放键值对的列表
key_values = []
# 遍历键值对
for key,value in dict_params.items():
    if value:
        # key_value = key + '=' + value
        key_value = '='.join([key,value])
    else:
        key_value = key

    # 把键值对放到一个列表里面
    key_values.append(key_value)

print(key_values)
params = '&'.join(key_values)
print(params)

