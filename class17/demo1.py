# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/27 21:33
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：Python获取时间
"""
import datetime

t = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
print(t)




