# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/21 20:43
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：逻辑运算
"""
# # 逻辑运算举例（多个条件的组合结果）
# x = int(input('请输入一个整数：'))
# print(x>1 and x < 10)

# # 取反
# b1 = False
# print(not b1)
#
# # or，只要一个条件满足就可以了
# # 假假为假，有真为真
# b2 = False
# print(b1 or b2)
#
# # and，必须两个条件同时满足
# # 真真为真，有假为假
# b3 = True
# b4 = True
# print(b4 and b3)

# 短路原则：当and 和 or可以通过左边的结果判断整个结果的时候，右边就不用执行了
# 短路原则可以提升执行效率
# 就相当于做单选题的时候，第一个选项已经确定是正确的了，后面的就可以不用看了
b3 = True
print(b3 or a and c)

b4 = False
print(b4 and a and c)


