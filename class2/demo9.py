# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/21 21:58
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：九九乘法表
"""
"""
x * y = xy
第一行：当x=1的时候，y从1循环到9
第二行：当x=2的时候，y从1循环到9
以此类推，当x从1循环到9的时候，每一个x的值，y都要从1到9，所以y循环可以写成内循环

打印倒三角：y<=x的时候打印，其他不打印
"""

x = 1
while x < 10:
    # 内部循环
    y = 1
    while y < 10:
        if y <= x:
            print(str(x)+'*'+str(y)+'='+str(x*y),end='\t')

        y += 1

    x += 1
    print()




