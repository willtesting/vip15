# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/21 20:09
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：作业回顾
"""

# input函数（交互式输入）
# 接收的输入，都是字符串类型
x = input("请输入一个整数：")
print(type(x))
print(int(x))

# 转换（输入不是整数的时候会报错）
x = int(x)
print(type(x))
