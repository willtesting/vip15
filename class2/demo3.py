# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/21 20:34
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：比较运算
"""
# 比较
x = 1
y = 1.0
print(x == y)

# 字符串比较（=、!=和包含）（>，<，>=，<=）
s1 = 'abc'
s2 = 'ab'
# 相等就是一模一样
print(s1 == s2)
# 从s1里面能够找到一个和s2一模一样的字符串
print(s1.__contains__(s2))

# 按位比较，按字符的ASCII码值比较
print(s1>s2)
