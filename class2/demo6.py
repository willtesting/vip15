# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/21 21:12
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：分支
"""

# 子过程，用缩进表示（缩进里面的代码属于缩进外面的部分）
if True:
    # 这个缩进叫子过程
    print('这里')
else:
    print('这里')

"""
z = |x| + |y|
用编程实现，当x、y取不同值时，求z的值，打印出来
当x、y取不同值用什么技术？input
|x|有几种情况？正：x；负：-x
|y|有几种情况？正：y；负：-y
z = |x| + |y|有几种情况？四种
正正，正负；负正，负负
x>=0 and y>=0; x>=0 and y<0; x<0 and y>=0; x<0 and y<0
x+y             x -y        -x +y       -x -y
"""
x = int(input('请输入x的值：'))
y = int(input('请输入y的值：'))
# if x >= 0 and y >= 0:
#     z = x + y
# elif x >= 0 and y < 0:
#     z = x - y
# elif x < 0 and y >= 0:
#     z = -x + y
# elif x < 0 and y < 0:
#     z = -x - y
#
# print(z)

if x >= 0:
    if y >= 0:
        z = x + y
    else:
        z = x - y
else:
    if y >= 0:
        z = -x + y
    else:
        z = -x - y

print(z)