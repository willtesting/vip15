# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/21 21:35
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：循环
"""

# 循环控制条件
i = 1
# 什么时候需要反复执行
while i <= 7:
    if i <= 5:
        # 这是要反复执行的
        print('星期'+str(i)+'上班')
    else:
        print('星期' + str(i) + '玩')
        # while使用continue一定要注意是否影响到控制条件的改变
        # continue

    # 改变循环控制条件
    i += 1


