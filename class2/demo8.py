# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/21 21:43
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：for循环
"""

# 缩写为range(10)
# start，end，step
# 区间是左闭右开[start,end)，start<=i<end
# step是每次循环，i改变的值（i+=step）
# i的初始值是0
# 当步长为负数：取值是start>=i>end
for i in range(0, 10, 1):
    print(i, end=',')
    # for循环里面改变i的值是不影响循环次数的
    i += 1

print()
for i in range(1, 8):
    if i <= 5:
        # 这是要反复执行的
        print('星期' + str(i) + '上班')
    else:
        print('星期' + str(i) + '玩')

    # 不能在循环里面改变步长
    i += 3


# 求100以内奇数的和
# 范围[0,100)
# 求和：每遍历一个都加到和里面

# 定义变量s，存放和
s = 0

for i in range(100):
    if i % 2 == 0:
        # 结束本次循环，进入下一次循环
        continue

    s += i

print(s)