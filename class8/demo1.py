# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/6 20:27
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：打开浏览器
"""
import time

from selenium import webdriver


# 打开浏览器
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
# 访问网站
driver.get('http://39.108.55.18:8000/Home/user/login.html')

# 定位元素
# find_element：当你的定位方式可能有多个元素的时候，只会返回第一个
# 如果没有，就会报错no such element
ele = driver.find_element('id','username')
# 获取元素属性
print(ele.get_attribute('outerHTML'))

# find_elements：把定位方式对应的所有元素存放到一个列表，然后返回出来
# 如果没有，返回一个空列表
eles = driver.find_elements('class name','text_cmu')
for ele in eles:
    print(ele.get_attribute('outerHTML'))

# ele = driver.find_element('partial link text','登')
# print(ele.get_attribute('outerHTML'))
ele = driver.find_element('xpath','//a[contains(text(),"登")]')
print(ele.get_attribute('outerHTML'))

ele = driver.find_element('css selector','#username')
print(ele.get_attribute('outerHTML'))

ele = driver.find_element('xpath','//*[@name="password"]')
print(ele.get_attribute('outerHTML'))

time.sleep(3)
