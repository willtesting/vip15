# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/6 21:41
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：Web自动化实战
"""
import time

from selenium import webdriver


driver = webdriver.Chrome()

# 添加隐式等待
# 原理：所有find_element都触发（去找元素，如果没有找到，等0.5s再去找；如果找到了，继续往下跑，
# 如果超过配置时间还没有找到，就报错
# 一定要配置隐式等待，因为它可以大大加强自动化的稳定性而不影响运行效率
driver.implicitly_wait(10)


driver.get('http://39.108.55.18:8000/Home/user/login.html')

# 定位，操作元素
ele = driver.find_element('xpath','//*[@id="username"]')
ele.send_keys('13800138006')
driver.find_element('xpath','//*[@id="password"]').send_keys('123456')
driver.find_element('xpath','//*[@id="verify_code"]').send_keys('1111')

driver.find_element('xpath','//*[@id="loginform"]/div/div[6]/a').click()

nickname = driver.find_element('xpath','//*[@class="red userinfo"]').text
print(nickname)
# 注销
driver.find_element('xpath','/html/body/div[1]/div/div/div/div[2]/a[2]').click()


driver.get('http://39.108.55.18:8000/Home/user/login.html')
# 定位，操作元素
ele = driver.find_element('xpath','//*[@id="username"]')
ele.send_keys('13800138006')
driver.find_element('xpath','//*[@id="password"]').send_keys('1234561')
driver.find_element('xpath','//*[@id="verify_code"]').send_keys('1111')

driver.find_element('xpath','//*[@id="loginform"]/div/div[6]/a').click()

# 校验提示
err_msg = driver.find_element('xpath','//*[@id="layui-layer1"]/div[2]').text
print(err_msg)


time.sleep(3)



