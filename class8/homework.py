# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/8 20:01
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：实现面向对象的浏览器类
"""
from selenium import webdriver


class Web:
    def __init__(self):
        # 为变量指定类型
        self.driver: webdriver.Chrome = None

    def openbrowser(self, br: str = 'gc'):
        """
        打开浏览器的方法
        :param br:
        :return:
        """
        if br == 'gc':
            self.driver = webdriver.Chrome()
        elif br == 'ff':
            self.driver = webdriver.Firefox()
        elif br == 'ed':
            self.driver = webdriver.Edge()
        else:
            # 默认打开chrome
            self.driver = webdriver.Chrome()

        # 添加隐式等待
        self.driver.implicitly_wait(10)
        # 最大化浏览器
        self.driver.maximize_window()


if __name__ == '__main__':
    web = Web()
    web.openbrowser('gc')
