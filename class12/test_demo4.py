# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/15 20:40
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：web测试用例
"""
import allure

from class11.webkeys import Web


# web:Web = None
#
# def setup():
#     global web
#     print('模块执行前置')
#     web = Web()
#     web.openbrowser('ed', '10')

@allure.feature('Web自动化')
class TestWeb:
    def setup_class(self):
        self.web = Web()
        self.web.openbrowser('ed', '10')

    @allure.story('会员中心')
    def test_login(self):
        allure.dynamic.title('登录')
        # 登录
        self.web.geturl('http://39.108.55.18:8000/Home/user/login.html')
        with allure.step('输入用户名'):
            self.web.input('//*[@id="username"]', '13800138006')

        self.web.input('//*[@id="password"]', '123456')
        self.web.input('//*[@id="verify_code"]', '{verify}')
        self.web.click('//*[@id="loginform"]/div/div[6]/a')
        nick = self.web.gettext('//*[@class="red userinfo"]')
        assert nick=='田小姐'

    @allure.story('会员中心')
    def test_userinfo(self):
        allure.dynamic.title('个人信息')
        # 点击个人中心
        self.web.move_and_click('//span[text()="账户设置"]', '//a[text()="个人信息"]')
        self.web.click('//*[@id="preview"]')
        self.web.intoiframe('//*[contains(@id,"layui-layer-iframe")]')
        # 上传文件
        # 批量写法
        # self.web.input('//*[@id="filePicker"]//input', '["will.png","small.png"]')
        # 单个文件上传
        self.web.input('//*[@id="filePicker"]//input', "will.png")
        self.web.waittext('//div[@class="info"]', '已上传')
        self.web.click('//*[@class="saveBtn"]')
        self.web.sleep('2')

    @allure.story('地址管理')
    def test_add_addr(self):
        allure.dynamic.title('新增收货地址')
        # 点击按钮的跳转，可以改成直接访问地址，这样对前置的依赖就少了
        self.web.geturl('http://39.108.55.18:8000/Home/User/address_list.html')
        self.web.click('//span[text()="增加新地址"]')

        # 输入信息
        self.web.input('//input[@name="consignee"]', '老will')
        self.web.input('//input[@name="mobile"]', '18888888888')
        # select标签选择
        # 把元素转成select对象
        self.web.select('//*[@id="province"]', '辽宁省')
        self.web.select('//*[@id="city"]', '1')
        self.web.select('//*[@id="district"]', '2')
        self.web.select('//*[@id="twon"]', '1')
        self.web.input('//input[@name="address"]', 'dsfkjdsalkjf')
        self.web.click('//*[@id="address_submit"]')
        self.web.sleep('2')

    @allure.story('地址管理')
    def test_del_addr(self):
        allure.dynamic.title('删除收货地址')
        # 删除收货地址
        self.web.click('//span[text()="老will"]/../..//a[text()="删除"]')
        self.web.sleep('2')

    def teardown_method(self):
        # 因为每一组用例跑完都要加截图
        allure.attach(self.web.driver.get_screenshot_as_png(),'用例结果截图',allure.attachment_type.PNG)