# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/15 20:35
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：测试类的前置和后置
"""


def setup():
    print('模块执行前置')


class TestDemo:
    def setup_class(self):
        print('整个类执行前')

    def setup_method(self):
        print('用例执行前')

    def test_case5(self):
        print('case5')

    def test_case6(self):
        print('case6')

    def teardown_method(self):
        print('用例执行后')

    def teardown_class(self):
        print('整个类执行后')


def teardown():
    print('模块执行后置')
