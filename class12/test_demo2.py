# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/15 20:30
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：pytest前置和后置
"""

def setup():
    print('模块执行前置')

def test_case3():
    print('case3')


def test_case4():
    print('case4')


def teardown():
    print('模块执行后置')
