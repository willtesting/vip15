# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/15 20:49
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：pytest断言和报错处理机制
"""
import pytest


class TestEx:

    def test_case7(self):
        a = 1
        assert a == 1
        int('a')
        # 如果你代码在执行过程中报错了，后面的也不跑了
        # 代码报错视为用例执行失败（后面步骤就不跑了）
        print('aaaaaaaaaaaaa')

    def test_case8(self):
        b = 3
        assert (b < 0) or (b > 5)
        # 一般来说断言后面不写代码（因为断言失败就不会跑了）
        int('a')

    def test_case9(self):
        c = 5
        # 在特定场景你可以主动让代码失败
        if c>6:
            pytest.fail('你指定的失败')

        print('cccccccccccc')