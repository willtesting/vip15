# -*- coding: utf-8 -*-
"""
@Time ： 2022/8/3 11:53
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：B栈验证码
"""
import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options

from class10.chaojiying import Chaojiying_Client

option = Options()
# 在打开浏览器之前，去掉自动化标识
option.add_experimental_option('excludeSwitches', ['enable-automation'])
option.add_argument('--disable-blink-features=AutomationControlled')

##关掉密码弹窗
prefs = {}
prefs['credentials_enable_service'] = False
prefs['profile.password_manager_enabled'] = False
option.add_experimental_option('prefs', prefs)

driver = webdriver.Chrome()
driver.implicitly_wait(10)

# 输入信息
driver.get('https://www.bilibili.com/')
driver.find_element('xpath', '//span[text()=" 登录 "]').click()
driver.find_element('xpath', '//input[@placeholder="请输入账号"]').send_keys('wuqingfqng1111')
driver.find_element('xpath', '//input[@placeholder="请输入密码"]').send_keys('djsfsdkla')
time.sleep(1)
driver.find_element('xpath', '//div[text()=" 登录 "]').click()

while True:
    time.sleep(2)
    # 截图
    ele = driver.find_element('xpath', '//div[@class="geetest_item_wrap"]')
    ele.screenshot('./bb.png')

    print(ele.size)
    # 处理起始位置
    xx = ele.size.get('width') // 2
    yy = ele.size.get('height') // 2

    # 创建对象
    verify = Chaojiying_Client('wuqingfqng', '6e8ebd2e301f3d5331e1e230ff3f3ca5', '904357')
    # 调用方法，获取验证码
    ver = verify.PostPic('./bb.png',9004).get('pic_str')
    print(ver)

    # 把验证码坐标解析成列表
    ver = ver.split('|')
    ver_loc = []
    for v in ver:
        v = v.split(',')
        ver_loc.append(v)

    print(ver_loc)

    # 点选坐标
    time.sleep(1)
    for x, y in ver_loc:
        ActionChains(driver).move_to_element_with_offset(ele, int(x) - xx, int(y) - yy).click().perform()
        time.sleep(1)

    # ActionChains(driver).move_to_element_with_offset(ele,0,0).click().perform()

    time.sleep(2)

    driver.find_element('xpath', '//div[text()="确认"]').click()

    # 重试
    try:
        time.sleep(2)
        ele = driver.find_element('xpath', '//img[@class="geetest_item_img"]')
        ele_size = ele.size
        print(ele_size)
        if ele_size.get('height') < 1:
            break
    except:
        break

time.sleep(3)
