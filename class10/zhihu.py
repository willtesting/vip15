# -*- coding: utf-8 -*-
"""
@Time ： 2022/8/1 17:10
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：知乎滑块验证码破解
"""
import time

import requests
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.edge.options import Options

from class10.slide import FindPic

option = Options()
option.add_argument(r'--user-data-dir=C:\Users\Amy\AppData\Local\Google\Chrome\User Data')
# 在打开浏览器之前，去掉自动化标识
option.add_experimental_option('excludeSwitches', ['enable-automation'])
option.add_argument('--disable-blink-features=AutomationControlled')
# 不自动关闭浏览器的配置
option.add_experimental_option("detach", True)

driver = webdriver.Edge(options=option)
driver.get('https://www.zhihu.com/signin?next=%2F')
driver.implicitly_wait(10)

# 显示验证码
driver.find_element('xpath','//div[text()="密码登录"]').click()
time.sleep(1)
driver.find_element('xpath','//input[@name="username"]').send_keys('i_wanting@yeah.net')
driver.find_element('xpath','//input[@name="password"]').send_keys('xiaobao168')

while True:
    time.sleep(1)
    try:
        driver.find_element('xpath', '//button[@type="submit"]').click()
    except:
        pass
    # 下载原图
    time.sleep(1)
    ele_bg = driver.find_element('xpath','//img[@class="yidun_bg-img"]')
    ele_bl = driver.find_element('xpath','//img[@class="yidun_jigsaw"]')
    src_bg = ele_bg.get_attribute('src')
    src_bl = ele_bl.get_attribute('src')
    print(src_bl)
    print(src_bg)
    # 下载
    con_bl = requests.get(src_bl).content
    con_bg = requests.get(src_bg).content
    # 保存二进制为图片
    file = open('block.jpg', mode='wb')
    file.write(con_bl)
    file.close()
    file = open('bg.jpg', mode='wb')
    file.write(con_bg)
    file.close()

    time.sleep(1)
    x = FindPic('./bg.jpg', './block.jpg')
    print(x)
    action = ActionChains(driver)
    action.click_and_hold(ele_bl).move_by_offset(x+8,0).release().perform()
    time.sleep(1)
    try:
        try:
            driver.find_element('xpath', '//button[@type="submit"]').click()
        except:
            pass
        ele_msg = driver.find_element('xpath','//div[contains(@class,"Notification-textSection")]')
        msg = ele_msg.text
        if msg.__contains__('10001') or msg.__contains__('账号'):
            break
    except:
        break

