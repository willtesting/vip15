# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/11 21:12
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：jd滑块验证码
"""
import base64
import time

import pyautogui
import requests
from selenium import webdriver
from selenium.webdriver import ActionChains

from class10.slide import FindPic

driver = webdriver.Edge()
driver.implicitly_wait(10)
driver.maximize_window()

driver.get('https://passport.jd.com/new/login.aspx')
driver.find_element('xpath', '//a[text()="账户登录"]').click()

driver.find_element('xpath', '//*[@id="loginname"]').send_keys('kldsjflsdka')
driver.find_element('xpath', '//*[@id="nloginpwd"]').send_keys('adslkfjsdf')
driver.find_element('xpath', '//*[@id="loginsubmit"]').click()

# 网页下载图片的两种方式
# 方式1，url地址下载
ele_url = driver.find_element('xpath', '//img[@alt="京东"]')
url = ele_url.get_attribute('src')
# 直接下载（得到图片的二进制码）
resp = requests.get(url)
# 图片的二进制码
# print(resp.content)
# 把二进制保存为图片
with open('logo.png', mode='wb') as f:
    f.write(resp.content)

while True:
    # 一直重试，直到验证码图片没有显示，则停止
    time.sleep(2)

    # 获取大图小图
    ele_big = driver.find_element('xpath', '//*[@class="JDJRV-bigimg"]/img')
    ele_small = driver.find_element('xpath', '//*[@class="JDJRV-smallimg"]/img')

    # 如果大图没有显示，则停止重试
    if not ele_big.is_displayed():
        break

    # 获取src属性
    src_big = ele_big.get_attribute('src')
    # print(src_big)
    src_small = ele_small.get_attribute('src')
    # 获取标准的base64字符串
    base64_big = src_big[22:]
    base64_small = src_small[22:]

    # base64图片保存
    with open('big.png', mode='wb') as f:
        f.write(base64.b64decode(base64_big))
    with open('small.png', mode='wb') as f:
        f.write(base64.b64decode(base64_small))

    x = FindPic('big.png', 'small.png')
    print(x)

    # 处理缩放
    x = int(x * 278 / 360)

    # # selenium自带滑动
    # action = ActionChains(driver)
    # # 按住小图
    # action.click_and_hold(ele_small)
    # # 开始拖动
    # action.move_by_offset(x,0)
    # # 松开鼠标
    # action.release()
    # # 让操作生效
    # action.perform()

    # 移动到小滑块的中心位置
    lo_small = [ele_small.location.get('x'), ele_small.location.get('y')]
    # 屏幕缩放
    lo_small[0] *= 1.25
    lo_small[1] *= 1.25
    # 处理位置
    lo_small[0] += 22
    lo_small[1] += 162
    pyautogui.moveTo(*lo_small)
    # 按住鼠标
    pyautogui.mouseDown(*lo_small)

    # 拖动距离也要处理屏幕缩放
    x = int(x * 1.25)

    # 先滑动过头
    pyautogui.moveTo(lo_small[0] + x + 20, lo_small[1] + 8, duration=0.3)
    # 再滑回去
    pyautogui.moveTo(lo_small[0] + x - 10, lo_small[1] - 2, duration=0.2)
    # 最后滑动到指定位置
    pyautogui.moveTo(lo_small[0] + x, lo_small[1] + 2, duration=0.2)
    # 松开鼠标
    pyautogui.mouseUp()

time.sleep(5)
