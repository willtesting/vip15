# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/11 21:31
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：opencv识别图片位置的示例代码
pip install opencv-python -i https://pypi.tuna.tsinghua.edu.cn/simple
"""
import cv2


def FindPic(target="./big.png", template="./small.png"):
    """
    找出图像中最佳匹配位置
    :param target: 目标即背景图
    :param template: 模板即需要找到的图
    :return: 返回最佳匹配及其最差匹配和对应的坐标
    """
    # 大图读出来做灰度处理
    target_rgb = cv2.imread(target)
    target_gray = cv2.cvtColor(target_rgb, cv2.COLOR_BGR2GRAY)

    # 读小图
    template_rgb = cv2.imread(template, 0)
    # 去匹配模板位置
    res = cv2.matchTemplate(target_gray, template_rgb, cv2.TM_CCOEFF_NORMED)
    # 返回两个匹配结果
    value = cv2.minMaxLoc(res)
    print(value)
    # 返回最佳匹配的x坐标
    return value[2][0]


if __name__ == '__main__':
    x = FindPic()
    print(x)
