# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/11 20:52
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：图文验证码破解
"""
import time

from selenium import webdriver

from class10.chaojiying import Chaojiying_Client

driver = webdriver.Edge()
driver.maximize_window()

# 添加隐式等待
# 原理：所有find_element都触发（去找元素，如果没有找到，等0.5s再去找；如果找到了，继续往下跑，
# 如果超过配置时间还没有找到，就报错
# 一定要配置隐式等待，因为它可以大大加强自动化的稳定性而不影响运行效率
driver.implicitly_wait(5)

# 登录
driver.get('http://39.108.55.18:8000/Home/user/login.html')
# 定位，操作元素
ele = driver.find_element('xpath', '//*[@id="username"]')
ele.send_keys('13800138006')
driver.find_element('xpath', '//*[@id="password"]').send_keys('123456')

# 截整个网页
driver.save_screenshot('page.png')

# 获取验证码元素
ele = driver.find_element('xpath','//*[@id="verify_code_img"]')
# 针对元素截图
ele.screenshot('verify.png')

# 调超级鹰破解验证码
chaojiying = Chaojiying_Client('wuqingfqng', '6e8ebd2e301f3d5331e1e230ff3f3ca5', '96001')
ver = chaojiying.PostPic('verify.png', 1902).get('pic_str')

driver.find_element('xpath', '//*[@id="verify_code"]').send_keys(ver)
time.sleep(3)
driver.find_element('xpath', '//*[@id="loginform"]/div/div[6]/a').click()

time.sleep(5)
