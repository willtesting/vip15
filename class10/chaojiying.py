#!/usr/bin/env python
# coding:utf-8

import requests
from hashlib import md5

class Chaojiying_Client(object):
    """超级鹰客户端调用类"""

    def __init__(self, username, password, soft_id):
        self.username = username
        self.password = password
        # md5加密的两行代码（如果你直接传md5，就可以注释这两行
        # password =  password.encode('utf8')
        # self.password = md5(password).hexdigest()
        print(self.password)

        self.soft_id = soft_id
        # 接口参数字典
        self.base_params = {
            'user': self.username,
            'pass2': self.password,
            'softid': self.soft_id,
        }
        # 接口请求头字典
        self.headers = {
            'Connection': 'Keep-Alive',
            'User-Agent': 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',
        }

    def PostPic(self, im, codetype=1004):
        """
        im: 图片字节，发送图片，并获取验证码结果
        codetype: 题目类型 参考 http://www.chaojiying.com/price.html
        """
        # 把图片读成二进制
        im = open(im, 'rb').read()
        params = {
            'codetype': codetype,
        }
        # 通过接口，破解验证码
        params.update(self.base_params)
        files = {'userfile': ('ccc.jpg', im)}
        r = requests.post('http://upload.chaojiying.net/Upload/Processing.php', data=params, files=files, headers=self.headers)
        return r.json()

    def ReportError(self, im_id):
        """
        im_id:报错题目的图片ID
        """
        params = {
            'id': im_id,
        }
        params.update(self.base_params)
        r = requests.post('http://upload.chaojiying.net/Upload/ReportError.php', data=params, headers=self.headers)
        return r.json()


if __name__ == '__main__':
    chaojiying = Chaojiying_Client('wuqingfqng', '6e8ebd2e301f3d5331e1e230ff3f3ca5', '96001')	#用户中心>>软件ID 生成一个替换 96001											#本地图片文件路径 来替换 a.jpg 有时WIN系统须要//
    print(chaojiying.PostPic('a.jpg', 1902))											#1902 验证码类型  官方网站>>价格体系 3.4+版 print 后要加()
    #print chaojiying.PostPic(base64_str, 1902)  #此处为传入 base64代码
