# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/4 20:01
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：装饰器作业
"""
import functools
import time


def my_time_new(t='s'):
    def outer(func):
        # 使用@functools.wraps(func)，让装饰器不改变原函数的属性
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            # 被装饰函数执行前
            t1 = time.time()
            res = func(*args, **kwargs)
            # 被装饰函数执行后
            t2 = time.time()
            if t=='s':
                print(func.__name__,'执行耗时',round(t2-t1,2),t)
            elif t=='ms':
                dt = round((t2-t1)*1000,2)
                print(f"{func.__name__}执行耗时：{dt} ms")
            else:
                print(func.__name__,'执行耗时',round((t2 - t1) * 1000000, 2), 'μs')
            # 把被装饰函数的结果原样返回
            return res

        return wrapper
    return outer


@my_time_new(t='ms')
def bubble_sort(height):
    # 控制比较的轮次
    # 为什么减1？因为剩下最后一个元素的时候，根本就不用比了
    for j in range(len(height) - 1):

        # 为什么-j？因为每一轮比较之后，最后一个元素都不参与下一轮比较了

        # 冒泡排序第一轮：遍历列表，两两比较如果顺序错误就交换
        # 为什么减1？因为两两比较，4个元素只需要比3次
        for i in range(len(height) - 1 - j):
            # 如果顺序错误就交换
            if height[i] > height[i + 1]:
                height[i], height[i + 1] = height[i + 1], height[i]


@my_time_new(t='ms')
def select_sort(height):
    # 控制比较的轮次
    # 为什么减1？因为剩下最后一个元素的时候，根本就不用比了
    for j in range(len(height) - 1):

        # 这里一定要-j？每一轮比较，最后一个元素都不用参与，否则一定选出来是上一轮最大的

        # 第一轮，选出最大的，放到列表末尾
        # 记录最大的下标（你选取了第一个元素作为最大）
        max_index = 0
        for i in range(1, len(height) - j):
            # 每次遍历都和最大的比较，如果元素比当前最大的大，就把最大的下标变成当前元素
            if height[i] > height[max_index]:
                max_index = i

        # 因为每一轮比较，剩下的最后一个都往前推一个
        # 选出最大的后，把它交换到末尾
        height[max_index], height[len(height) - 1 - j] = height[len(height) - 1 - j], height[max_index]


def quick_sort(list1, left, right):
    """
    快排
    :param list1: 待排序列表
    :param left: 左边界
    :param right: 右边界
    :return:
    """

    # 选取待排序元素最右边为基准
    base = list1[right]

    # 初始化游标的位置
    l, h = left, right

    while l < h:
        while l < h:
            # 从左往右找比基准大的
            if list1[l] > base:
                # 如果找到了
                # 交换到h位置
                list1[l], list1[h] = list1[h], list1[l]
                # h 往左移动一位
                h -= 1
                # 交换位置
                break
            else:
                # 如果没找到呢？l往右移动一位继续找
                l += 1

        while l < h:
            if list1[h] < base:
                # 交换到l位置
                list1[l], list1[h] = list1[h], list1[l]
                l += 1
                # 交换位置
                break
            else:
                h -= 1

    # 左边递归（出口是什么？）
    if l <= left + 1:
        # 当左边小于等于一个元素的时候，不用递归了
        pass
    else:
        quick_sort(list1, left, l - 1)

    if h >= right - 1:
        pass
    else:
        quick_sort(list1, h + 1, right)


if __name__ == '__main__':
    # 把这个模块作为主运行入口（右键run这样）执行的时候，才会运行
    # import导入是不执行这里的
    list1 = [i for i in range(1999, 0, -1)]
    bubble_sort(list1)

    list1 = [i for i in range(1999, 0, -1)]
    select_sort(list1)

    # 调整堆栈大小
    import sys
    sys.setrecursionlimit(10 ** 6)
    t1 = time.time()
    list1 = [i for i in range(1999, 0, -1)]
    quick_sort(list1,0,len(list1)-1)
    t2 = time.time()
    print(round((t2-t1)*1000,2))

