# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/1 20:44
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：装饰器
"""
# 把函数传给变量
import time


def func1():
    print('hello will')


# 递归类函数不建议使用装饰器，因为每次递归都是一次调用，都会执行装饰器
# 两层装饰器
def my_time(func):
    """
    如果函数使用了装饰器，装饰器内部必须要调用函数
    把被装饰函数传进来
    :param func:
    :return:
    """
    # 这里是执行模块的时候就会执行
    # print(func.__name__)
    # 闭包函数
    def wrapper(*args, **kwargs):
        # 调用函数才会执行
        # 被装饰函数执行前
        t1 = time.time()
        # 这里是真正调用被装饰函数的时候
        res = func(*args, **kwargs)

        # 被装饰函数执行后
        t2 = time.time()
        print(t2-t1)

        return res

    # 这里的闭包调用不要带括号
    return wrapper


# 输入main回车，快速敲出这个if
if __name__ == '__main__':
    # 只有右键run当前这个py的时候，才会执行这里的代码
    # 这个模块是运行主入口的时候才执行
    # 可以作为测试代码，示例代码
    f = func1
    # func1()
    f()
