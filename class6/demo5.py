# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/1 21:56
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字符串格式化
"""
a = 20.11 * False
# 怎么显示0.00
print('%.2f' % a)

name = 'will'
age = 18
s = '名字是:' + name + '，年龄是:' + str(age)
print(s)

# 使用字符串格式化
s = f'名字是:{name}，年龄是:{age}'
print(s)

# 指定输出位数
print('{:4}a'.format(0.0))

print('abc%6.2f' % (0.0,))

# 小数前后都可以补0
print("PI=%07.03f" % 5.2)
