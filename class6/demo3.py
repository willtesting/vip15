# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/1 21:32
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：导入
"""
# 不推荐
# from . import decs

# 推荐使用的导入方式
from class6.decs import my_time
# 导入的时候，会先运行被导入的模块

from class6 import decs
from class6.demo2 import my_time as func_mytime

# @my_time
# def func1():
#     print('测试导入')
#
#
# func1()
# func_mytime()
