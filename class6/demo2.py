# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/1 21:11
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：三层装饰器
"""
import time
# 不允许出现循环导入
# from class6.demo3 import func1


def my_time():
    print('测试导入重命名')

# 如果装饰器本身接收参数，必须要在使用的带括号
# 如果装饰器本身不接收参数，使用的时候一定不要带括号
def my_time_new(t='s'):
    def outer(func):
        def wrapper(*args, **kwargs):
            # 被装饰函数执行前
            t1 = time.time()
            print('my_time_new')
            res = func(*args, **kwargs)
            # 被装饰函数执行后
            t2 = time.time()
            if t=='s':
                print(func.__name__,'执行耗时',round(t2-t1,2),t)
            elif t=='ms':
                print(round((t2-t1)*1000,2),t)
            else:
                print(func.__name__,'执行耗时',round((t2 - t1) * 1000000, 2), 'μs')
            # 把被装饰函数的结果原样返回
            return res

        return wrapper
    return outer

