# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/18 21:27
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：基础数据类型
"""
# 宏观来讲，Python是两种基础数据类型（数字类型和字符类型）
# 微观来讲，数字类型又分（整数，小数，布尔值）

# 整数（int)
x = 1
print(type(x))
# 大整数
y = 324987329847328947329847983274982374923748923174982374198
print(y)

# 小数（浮点数：float)
x = 1.0
print(type(x))
# 双精度（double型小数：它可以表示十进制的15或16位有效数字）
print(11/3)

# 布尔值（bool）
x = True
print(x)

# 字符串(str)
s = '你好啊1,都是开发静安寺的'
print(s)





