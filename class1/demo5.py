# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/18 21:36
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：基础数据类型的互转与互斥
"""
# 互转：一种数据类型转成另一种

# 数字类型都是可以从一种数据类型使用关键字直接转成另一种数据类型的
x = 1
print(float(x))
y = 1.1
print(int(y))
# 为什么说bool值是一个数字类型
z = False
print(int(z))
z = True
print(int(z))
# 默认有浮点数就使用浮点数
x = 1 + 1.0
print(x)

# 数字都可以转成字符串（因为数字是字符串子集）
x = 11
s = str(x)
print(type(s))
# 字符串不一定能转成数字（因为字符串里面可能有字母）
# 一定要刚好是一个数字的字符串才能转成数字，否则会报错
s = '11.1'
print(float(s))


# 互斥（在运算过程中，字符串和数字类型不能直接运算）
x = 'a' + str(3)
print(x)
x = int('1') + 1
print(x)

# 数字类型是可以直接运算的
y = 1 + 1.1 + False
print(y)

