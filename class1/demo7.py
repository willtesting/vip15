# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/18 22:00
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：面向过程（以当前正在发生什么为顺序进行编程）
处理一个具体逻辑的时候，基本都是面向过程
"""
"""
接力赛
A跑完一圈34s，B跑完一圈30s，C跑完一圈38s
每次接棒需要2s
请问本次接力赛的成绩是多少？
"""
A = 34
B = 30
C = 38

# 先算A，B接力，再算B，C接力
score = A + 2 + B + 2 + C
print(score)



