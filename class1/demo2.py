# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/18 21:06
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：基础语法1
"""
# 定义两个变量
x, y = 1, 2
print(x, y)

# 赋值，把一个值给一个变量
z = 1
print(z)

# 小括号（元组，运算优先级，函数的参数部分）
x = (5 + 3) * 4
print(x)

# 字符串
print('你好')
print("will")

# +，-，*，/
# %（取余）
print(13 % 3)

# 幂运算(2的5次方）
print(2**5)

# 整除（//）
print(11//3)

# 赋值叠加 +=
x = 1
# x = x + 3
x += 3
print(x)

# 比较运算
print(3>5)