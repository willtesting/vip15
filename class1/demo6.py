# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/18 21:48
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：关于bool值为False的情况
"""
# 空类型（特殊类型，叫空类型）
x = None
print(x)
# # 与所有类型都互斥
# print(x + 1)

# 但是可以转成字符串
print(str(x) + '1')

# 五种类型和bool值的关系
x = 0.000001
print(bool(x))
# 所有的空和零：None，''，0，0.0，[]，()，{}
print(bool(None), bool(''), bool(0), bool(0.0), bool([]), bool(()), bool({}))
# 其他情况都是True
# 主要用于  if x:   这种写法

