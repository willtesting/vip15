# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/23 21:45
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字典与json的关系
"""
# 数据结构之间的混合存放
# 数据结构的元素可以是任意类型
list_1 = [[2, 3], {'a': 1, 'b': '2'}, (1, 2, 3), 1, 1.1, '1', False]
dict_1 = {'a': [1, 3, 4], 'b': (1, 2), 'c': False}

print(list_1[1].get('b'))
print(dict_1.get('a')[2])

import json

# json字符串（通用编程语言里面，列表和字典数据结构的字符串体现）
# json.dumps把数据结构转化为json字符串
print(json.dumps(list_1))
print(json.dumps(dict_1))

s = '{"code":"Success","data":{"continent":"亚洲","country":"中国",' \
    '"zipcode":"410006","timezone":"UTC+8","accuracy":"区县","owner":' \
    '"中国电信","isp":"中国电信","source":"数据挖掘","areacode":"CN",' \
    '"adcode":"430104","asnumber":"4134","lat":"28.200043",' \
    '"lng":"112.896852","radius":"21.1458","prov":"湖南省",' \
    '"city":"长沙市","district":"岳麓区"},"charge":true,"msg":"查询成功",' \
    '"ip":"113.218.168.223","coordsys":"WGS84"}'

print(s)

res = json.loads(s)
print(res)

