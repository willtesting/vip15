# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/23 21:38
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字典的遍历
"""
my_dict = {'name': 'will', 'age': 18, 'height': '170cm'}

# 字典的遍历，本质还是列表的遍历
# 获取键并转为列表
keys = list(my_dict.keys())
print(keys)

# 键的遍历
for key in my_dict.keys():
    print(key, ':', my_dict.get(key))

# 值的遍历
for value in my_dict.values():
    print(value)

# 键值对的遍历
for item in my_dict.items():
    print(item)

# 键和值的同时遍历
for key, value in my_dict.items():
    print(key, value)
