# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/23 21:15
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：元组
"""
# 定义：关键字tuple
tup1 = ()
print(tup1)

tup2 = (1,)
print(tup2)

tup3 = [i for i in range(5)]
# 列表转元组（互转）
tup3 = tuple(tup3)
print(tup3)
print(list(tup3))

# # 不能更改
# tup3[1] = 33

# 拼接：得到一个新的元组
tup3 = tup2 + tup3
print(tup3)

# 只能读
for t in tup3:
    print(t)

# 应用在程序里面，不希望数据被其他位置代码更改的数据
