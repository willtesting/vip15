# -*- coding: utf-8 -*-
"""
@Time ： 2023/3/21 20:00
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
# 集合，set（集合里面重复的数据会自动去掉）
my_set = {1, 2, 3, 4, 4, 4, 4}
print(my_set)

# 列表去重
mylist = [1, 1, 2, 3, 4, 4, 5, 'a', 'a']
mylist = list(set(mylist))
print(mylist)


