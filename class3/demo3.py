# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/23 20:35
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：列表的特殊用法
"""

# list_1 = []
# for i in range(1000):
#     list_1.append(1)
#
# print(list_1)

# 定义列表的推导式
list_1 = [i for i in range(10)]
print(list_1)

# # 下标超出范围，叫下标越界
# for i in range(len(list_1)):
#     print(list_1[i])
#     list_1.pop(i)

list_2 = [11,12,13]

# 列表的拼接
list_3 = list_1 + list_2
print(list_3)

# 字符串可以看作一个字符列表
s = 'ab' + 'cd'
print(s)

# 列表的截取（得到一个新列表）
print(list_3[1:5])
print(list_3)

# list_3[start:end:step]
# start：默认是0，代表开始下标
# end：默认是len(list_3)，代表列表的末尾
# step：默认是1，代表正向一个一个截取，负数代表反向
# 截取的区间是左闭右开start<=i<end

# list_3[:]
print(list_3[0:len(list_3)])

# 步长
print(list_3[::2])
# 负数代表反向
print(list_3[::-1])

# 负数下标
# 下标为负数的时候，其实是反向下标，从-1
print(list_3[-2])

# 负数代表反向
# 用负数下标表示
print(list_3[-1:-len(list_3)-1:-1])
# 用正数下标表示
print(list_3[len(list_3)-1:None:-1])
