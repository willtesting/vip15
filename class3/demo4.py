# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/23 20:57
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：遍历
"""
# 列表，代表学生的年龄
list_1 = [18, 21, 19, 22, 20]

# 把每一个学生年龄+1

# 下标遍历（读写）
# 不建议在列表遍历的同时，新增或者删除列表的元素
for i in range(len(list_1)):
    # print(list_1[i])
    list_1[i] += 1

print(list_1)

# 成员遍历（只读）
for s in list_1:
    print(s)
    s += 1

print(list_1)