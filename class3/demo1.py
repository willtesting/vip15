# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/23 20:11
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：变量
"""
# 定义变量
a = 3
b = 3
print(id(a))
print(id(b))
b = '4a'
print(id(a))
print(id(b))

# 存
b = 4
# 取（直接使用变量，其实是使用变量的值）
b = a + 1
print(b)

# 以字母开始的字符串（一般只包含字母数字，下划线）
a_1 = 2
# 单词
name = 'will'
# 单/双驼峰/下划线
yourName = 'roy'
YourName = 'kaka'
your_name = 'tufei'
