# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/23 20:22
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：列表
"""

# 列表的关键字是list
# 定义一个空列表
list_1 = []
print(bool(list_1))
# 初始化一个有数据的列表
list_2 = [1, 2, 3,'4','4']

# 使用一个元素值
print(list_2[0])

# 插入（在末尾添加）
list_2.append('5')
print(list_2)
# 在指定位置插入
list_2.insert(1,11)
print(list_2)

# 改变元素值
list_2[1] = 111
print(list_2)

# 删除
# 按下标删除
list_2.pop(1)
print(list_2)
# 按值删除，只会删除第一个（如果值不存在会报错）
list_2.remove('4')
print(list_2)
# 删掉某个元素
del list_2[3]
print(list_2)

# 获取列表的长度
print(len(list_2))

# 清空列表
list_2.clear()
print(list_2)


