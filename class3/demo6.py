# -*- coding: utf-8 -*-
"""
@Time ： 2023/8/23 21:24
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：字典
"""
# 定义，关键字：dict
# 快速注释/取消注释，Ctrl+/
# 快速格式化Ctrl+Alt+Shift+L
dict_1 = {}
dict_2 = {1: 2, "2": 3,'age': 88}

# 一般来说，我们要求，键是字符串，因为键是有意义的
my_dict = {'name': 'will', 'age': 18, 'height': '170cm'}
print(my_dict)

# 读（按键读）
# 键不存在返回None
print(my_dict.get('name'))
# 键不存在会报错
print(my_dict['name'])

# 改
# 改变已有键的值
my_dict['name'] = '老Will'
print(my_dict)

# 如果改变的键不存在，那么就是新增一个键值对
my_dict['name2'] = '老Will'
print(my_dict)

# 按键删除键值对（如果pop的键不存在会报错）
# del my_dict['name2']
my_dict.pop('name2')
print(my_dict)

# 删除最后一个键（返回这个键值对：元组）
s = my_dict.popitem()
print(s)
print(my_dict)

# 清空字典
my_dict.clear()
print(my_dict)

# 一般来说，我们要求，键是字符串，因为键是有意义的
my_dict = {'name': 'will', 'age': 18, 'height': '170cm'}
print(my_dict)

# 合并两个字典（把dict_2和并到my_dict里面去）
my_dict.update(dict_2)
print(my_dict)

