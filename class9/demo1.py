# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/8 20:08
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：主流程
"""
import re
import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome()
driver.maximize_window()
# 添加隐式等待
# 原理：所有find_element都触发（去找元素，如果没有找到，等0.5s再去找；如果找到了，继续往下跑，
# 如果超过配置时间还没有找到，就报错
# 一定要配置隐式等待，因为它可以大大加强自动化的稳定性而不影响运行效率
driver.implicitly_wait(5)

# 登录
driver.get('http://39.108.55.18:8000/Home/user/login.html')
# 定位，操作元素
ele = driver.find_element('xpath', '//*[@id="username"]')
ele.send_keys('13800138006')
driver.find_element('xpath', '//*[@id="password"]').send_keys('123456')
driver.find_element('xpath', '//*[@id="verify_code"]').send_keys('1111')
driver.find_element('xpath', '//*[@id="loginform"]/div/div[6]/a').click()
# # 获取提示
# msg = driver.find_element('xpath','//*[@id="layui-layer1"]/div[2]').text
# print(msg)

nickname = driver.find_element('xpath','//*[@class="red userinfo"]').text
print(nickname)

# element click intercepted
# driver.refresh()
# driver.find_element('xpath','//*[@id="loginform"]/div/div[6]/a').click()

# 个人信息
# # 方式1选择xpath的第几个元素（下标从1开始）
# driver.find_element('xpath','(//a[text()="个人信息"])[2]').click()

# 方式2，鼠标操作
# 创建一个鼠标操作的对象
action = ActionChains(driver)
# 鼠标移动到元素
action.move_to_element(driver.find_element('xpath', '//span[text()="账户设置"]'))
# 使所有鼠标操作生效
action.perform()
driver.find_element('xpath', '//a[text()="个人信息"]').click()

driver.find_element('xpath', '//*[@id="preview"]').click()

iframe = driver.find_element('xpath','//*[contains(@id,"layui-layer-iframe")]')
driver.switch_to.frame(iframe)

# 上传文件
driver.find_element('xpath', '//*[@id="filePicker"]//input') \
    .send_keys(r'C:\Users\Amy\Desktop\will.png')

# 固定等待
# time.sleep(1)

# 显示等待
from selenium.webdriver.support import expected_conditions as EC
# 每隔0.5s判断一次，直到10s，就报错
WebDriverWait(driver,10,0.5).until(
    # 判断条件，是//div[@class="info"]元素的文本内容包含：已上传1份
    EC.text_to_be_present_in_element((By.XPATH,'//div[@class="info"]'),'已上传')
)

driver.find_element('xpath','//*[@class="saveBtn"]').click()
time.sleep(2)

# 点击按钮的跳转，可以改成直接访问地址，这样对前置的依赖就少了
driver.get('http://39.108.55.18:8000/Home/User/address_list.html')
driver.find_element('xpath','//span[text()="增加新地址"]').click()

# 输入信息
driver.find_element('xpath','//input[@name="consignee"]').send_keys('老will')
driver.find_element('xpath','//input[@name="mobile"]').send_keys('18888888888')

# select标签选择
# 把元素转成select对象
select = Select(driver.find_element('xpath','//*[@id="province"]'))
select.select_by_index(3)
time.sleep(1)
select.select_by_value('4670')
time.sleep(1)
select.select_by_visible_text('辽宁省')

time.sleep(0.5)
select = Select(driver.find_element('xpath','//*[@id="city"]'))
select.select_by_index(1)

time.sleep(0.5)
select = Select(driver.find_element('xpath','//*[@id="district"]'))
select.select_by_index(2)

time.sleep(0.5)
select = Select(driver.find_element('xpath','//*[@id="twon"]'))
select.select_by_index(1)

driver.find_element('xpath','//input[@name="address"]').send_keys('dsfkjdsalkjf')
driver.find_element('xpath','//*[@id="address_submit"]').click()

time.sleep(2)
# 删除收货地址
driver.find_element('xpath','//span[text()="老will"]/../..//a[text()="删除"]').click()
time.sleep(2)

# 搜索
driver.find_element('xpath','//*[@id="q"]').send_keys('手机')
driver.find_element('xpath','//*[@id="sourch_form"]/a').click()

# 加购物车
driver.find_element('xpath','//a[contains(text(),"迷你手机456")]/../..//a[text()="加入购物车"]').click()
driver.find_element('xpath','//span[@class="layui-layer-setwin"]').click()
driver.find_element('xpath','//span[text()="我的购物车"]').click()

driver.find_element('xpath','//a[text()="去结算"]').click()

driver.find_element('xpath','//button[@class="checkout-submit"]').click()

order_info = driver.find_element('xpath','//p[@class="succ-p"]').text
print(order_info)
orderid = re.findall('\d{18}',order_info)[0]
print(orderid)

# 订单号，订单查询
# 点击我的订单是OK的，结合我们之前的运行情况，发现也确实打开了新的我的订单页面
driver.find_element('xpath','//a[text()="我的订单"]').click()

# 获取所有窗口的handleid，是一个列表
handles = driver.window_handles
print(handles)


# 按下标切换
# # 通过handleid就可以切换到指定窗口
# print(driver.title)
# driver.switch_to.window(handles[1])
# print(driver.title)

# 按title切换
title = '我的订单'
# 遍历所有的窗口id
for h in handles:
    # 每切换一个窗口，就获取title
    driver.switch_to.window(h)
    t = driver.title
    # 如果当前窗口的标题包含需要的标题，就不再切换
    if t.__contains__(title):
        break



# 输入订单，搜索
driver.find_element('xpath','//*[@id="search_key"]').send_keys(orderid)
driver.find_element('xpath','//*[@id="search_order"]/input[2]').click()

time.sleep(13)
