# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/11 20:03
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
import re

order_info = '订单号：  202309112001532098    |     付款金额（元）：  1818.00 元'
# 使用正则提取订单号
orderid = re.findall('\d{18}',order_info)[0]
print(orderid)


s = 'data:image/png;base64,iVBORw0KGgo'
print(s[22:])
