# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/18 20:55
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：mysql基础操作
"""
import pymysql


conf = {
    'user': 'inter',
    'password': 'test123456',
    # 主机和端口
    'port': 3306,
    'host': '39.108.55.18',
    # 数据库名称
    'db': 'test_project',
    # 编码，默认utf8
    'charset': 'utf8',
}

# 创建连接，执行语句的时候是在这个连接上执行
connect = pymysql.connect(**conf)
# 获取游标
cursor = connect.cursor()

# # 用游标执行SQL
# cursor.execute('select * from userinfo where username like "Will%";')
# print(cursor.description)
# res = cursor.fetchall()
# print(res)

cursor.execute('delete from userinfo where `describe` like "%测试账号%"')
# 除了查询，都需要commit去执行
connect.commit()

cursor.close()
connect.close()



