# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/18 20:33
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：随机数和时间戳的拓展
"""
import random

# 随机数
i= random.randint(0,999999)
# i = '%06d' %i
print(i)

import time

# 时间戳
t = time.time()
t = int(t * 1000)
print(t)

# 随机手机号
# 先随机出前两位
p = f'1{random.randint(3,9)}'
# 然后随机后9位
h = random.randint(0,999999999)
h = '%09d' % h
# 最后拼起来
p += h
print(p)






