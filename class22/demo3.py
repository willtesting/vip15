# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/18 21:29
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：天气接口
"""
import jsonpath
import requests

resp = requests.get('https://qifu-api.baidubce.com/ip/geo/v1/district?ip=113.246.185.105')
print(resp.text)
json_res = resp.json()

json_res = {"code": "Success",
            "data": [{"continent": "亚洲", "country": "中国", "zipcode": "410013", "timezone": "UTC+8", "accuracy": "区县",
                     "owner": "中国电信", "isp": "中国电信", "source": "数据挖掘", "areacode": "CN", "adcode": "430104",
                     "asnumber": "4134", "lat": "28.200043", "lng": "112.896852", "radius": "21.1458", "prov": "湖南省",
                     "city": "长沙市1", "district": "岳麓区"},{"continent": "亚洲", "country": "中国", "zipcode": "410013", "timezone": "UTC+8", "accuracy": "区县",
                     "owner": "中国电信", "isp": "中国电信", "source": "数据挖掘", "areacode": "CN", "adcode": "430104",
                     "asnumber": "4134", "lat": "28.200043", "lng": "112.896852", "radius": "21.1458", "prov": "湖南省",
                     "city": "长沙市", "district": "岳麓区"}], "charge": True, "msg": "查询成功", "ip": "113.246.185.105",
            "coordsys": "WGS84"}

city = jsonpath.jsonpath(json_res, 'data.[2].city')
print(city)
