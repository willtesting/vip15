# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/25 20:53
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
import os

from class16.Logger import logger, path
from class16.excel_ddt import ddt
from class17.mail import Mail
from class17.result import Result

case_name = '电商项目用例'
ddt.run_web_case(f'lib/cases/{case_name}.xlsx',f'lib/cases/result-{case_name}.xlsx')

# 分析Excel结果里面数据
res = Result()
summary = res.get_res(f'lib/cases/result-{case_name}.xlsx')
logger.info(summary)
groups = res.get_groups(f'lib/cases/result-{case_name}.xlsx')
logger.info(groups)

# 发邮件
mail = Mail()
# 附件代码
mail.mail_info['filepaths'] = [f'lib\\cases\\result-{case_name}.xlsx']
mail.mail_info['filenames'] = [f'result-{case_name}.xlsx']
with open(path + 'lib\\' + mail.mail_info.get('mailmodule'), mode='r', encoding='utf8') as f:
    html = f.read()

# 替换汇总报告数据
for key in summary:
    html = html.replace(key, str(summary.get(key)))
# 状态的颜色替换
if summary.get('status') == 'FAIL':
    html = html.replace('scolor', 'red')
else:
    html = html.replace('scolor', 'green')

# 一行的tr源码
ele_tr = '<tr><td width="100" height="28" align="center" bgcolor="#FFFFFF" style="border:1px solid #ccc;">分组信息</td>' \
         '<td width="80" height="28" align="center" bgcolor="#FFFFFF" style="border:1px solid #ccc;">用例总数</td>' \
         '<td width="80" align="center" bgcolor="#FFFFFF" style="border:1px solid #ccc;">通过数</td>' \
         '<td width="80" align="center" bgcolor="#FFFFFF" style="border:1px solid #ccc; color: scolor">状态</td></tr>'

# 获取分组统计的所有tr
trs = ''
for g in groups:
    one_tr = ele_tr
    one_tr = one_tr.replace('分组信息', str(g[0]))
    one_tr = one_tr.replace('用例总数', str(g[1]))
    one_tr = one_tr.replace('通过数', str(g[2]))
    one_tr = one_tr.replace('状态', str(g[3]))
    # 状态的颜色替换
    if g[3] == 'FAIL':
        one_tr = one_tr.replace('scolor', 'red')
    else:
        one_tr = one_tr.replace('scolor', 'green')
    trs += one_tr

html = html.replace('mailbody', trs)

mail.send(html)

# 生成可发布的报告
os.system('allure generate result -o report --clean')
