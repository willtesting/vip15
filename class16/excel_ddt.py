# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/25 20:50
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：Excel数据读成三维列表，并调用执行
"""
import datetime
import os

import pytest

from class11.webkeys import Web
from class15.Excel import Reader, Writer
from class16.Logger import path, logger


class DDT:

    def __init__(self):
        """初始化"""
        # 测试用例集合
        self.test_suite = []
        # 测试用例
        self.test_case = []
        # 关键字对象
        self.web: Web = None

        # 为test_suite编号
        self.suite_idx = 0

        # 记录feature和story
        self.feature = 'sheet'
        self.f_idx = 0
        self.story = 'test_suite'
        self.s_idx = 0

        # 写入结果的writer对象
        self.writer = Writer()

    def run_test_suite(self):
        self.suite_idx += 1
        # 重命名：把test_web这个文件按运行的下标重命名
        os.rename(path + rf'class16\test_web_{self.suite_idx - 1}.py',
                  path + rf'class16\test_web_{self.suite_idx}.py')
        pytest.main(['-s', path + rf'class16\test_web_{self.suite_idx}.py', '--alluredir', './result'])

    def run_web_case(self, case_path='lib/cases/电商项目用例.xlsx', result_path='lib/cases/result-电商项目用例.xlsx'):
        """
        跑web自动化用例
        :return:
        """
        # 创建关键字对象
        self.web = Web()

        # 读成三维列表
        reader = Reader()
        # 打开一个excel
        # 处理成绝对路径
        case_path = path + case_path
        result_path = path + result_path

        reader.open_excel(case_path)
        self.writer.copy_open(case_path, result_path)
        # 获取所有sheet
        sheets = reader.get_sheets()
        # 先在第一个sheet写入开始时间
        start_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.writer.set_sheet(sheets[0])
        self.writer.write(1,3,start_time)


        try:
            for sheet in sheets:
                # 设置读取的sheet页面（读写统一）
                reader.set_sheet(sheet)
                self.writer.set_sheet(sheet)
                # 设置写入行，从第二行开始
                self.writer.row = 1

                logger.info(sheet)
                self.feature = sheet
                self.f_idx += 1
                # 读取当前sheet的所有行
                lines = reader.readline()

                # 第一行不需要
                for line in lines[1:]:
                    if len(line[0]) > 0:
                        # 这是test_suite
                        if self.test_suite:
                            # 如果有test_case，先保存
                            if self.test_case:
                                self.test_suite.append(self.test_case)

                            # 执行上一组用例
                            logger.info('这里执行：')
                            logger.info(self.test_suite)
                            self.run_test_suite()

                        # 这是test_suite：创建一个新的test_suite和test_case
                        self.test_suite = []
                        self.test_case = []
                        self.story = line[0]
                        self.s_idx += 1
                        # 写入的行数加1
                        self.writer.row += 1

                    elif len(line[1]) > 0:
                        # 这是test_case：
                        if self.test_case:
                            # 说明有上一组用例，需要保存上一组用例
                            self.test_suite.append(self.test_case)

                        # 创建一个新的测试用例
                        self.test_case = []
                        # 把用例那一行也加进去
                        self.test_case.append(line)

                    else:
                        # 测试步骤：加入到测试用例里面
                        self.test_case.append(line)

                # 当读完一个sheet页面：要执行最后一组test_suite
                if self.test_case:
                    # 如果有test_case，先保存
                    self.test_suite.append(self.test_case)
                    logger.info('这里执行：')
                    logger.info(self.test_suite)
                    self.run_test_suite()

                    # 这是test_suite：创建一个新的test_suite和test_case
                    self.test_suite = []
                    self.test_case = []

        except Exception as e:
            logger.exception(e)

        finally:
            # 最终跑完，把文件名字改回去
            os.rename(path + rf'class16\test_web_{self.suite_idx}.py',
                      path + r'class16\test_web_0.py')

            # 在第一个sheet写入结束时间
            end_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            self.writer.set_sheet(sheets[0])
            self.writer.write(1, 4, end_time)

            # 一定要保存写入的结果
            self.writer.save_close()


# 使用全局变量，创建一个对象，方便引用
ddt = DDT()
