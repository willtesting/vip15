# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/18 20:07
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：测试模块-登录+注册
"""
import allure
import pytest
from class16.Logger import logger
from class16.excel_ddt import ddt


@allure.feature(f'#{ddt.f_idx} {ddt.feature}')
class TestWeb:

    def jie_tu(self, driver, title):
        try:
            allure.attach(driver.get_screenshot_as_png(), title, allure.attachment_type.PNG)
        except Exception as e:
            allure.attach(e.__str__(), '错误信息', allure.attachment_type.TEXT)

    @allure.step
    def run_case_new(self, line, obj):
        """
        反射执行
        :param line: 用例
        :param obj: 关键字对象
        :return:
        """
        key = line[3]
        # 反射获取关键字函数
        func = getattr(obj, key)
        # 获取参数
        params = line[4:7]
        idx = -1
        # 获取最后一个不为空的下标
        for i in range(len(params) - 1, -1, -1):
            if params[i]:
                idx = i
                break

        params = params[:idx + 1]
        # 返回关键字执行结果，便于写入Excel
        return func(*params)

    @pytest.mark.parametrize('params', ddt.test_suite)
    @allure.story(f'#{ddt.s_idx} {ddt.story}')
    def test_case(self, params):
        """一个功能点，即是一个用例"""
        logger.info(params)
        # 报告定制用例名
        allure.dynamic.title(params[0][1])
        # 指定用例title，用例行就不写了，所以行数加1
        ddt.writer.row += 1

        for i in range(1, len(params)):
            line = params[i]
            with allure.step(line[2]):
                try:
                    res = self.run_case_new(line, ddt.web)
                    # 处理类似断言的关键字
                    if type(res) == tuple and res[0] is False:
                        # 写入失败的结果
                        ddt.writer.write(ddt.writer.row, 7, 'FAIL', 2)
                        ddt.writer.write(ddt.writer.row, 8, str(res))
                        # 运行失败后，行数就不是加1了！应该是加剩下没跑的步骤数量
                        ddt.writer.row += len(params) - i
                        # 失败截图
                        self.jie_tu(ddt.web.driver, '失败截图')
                        # 主动让用例失败
                        pytest.fail(str(res))

                    # 写入成功的结果
                    ddt.writer.write(ddt.writer.row, 7, 'PASS', 3)
                    ddt.writer.write(ddt.writer.row, 8, str(res))
                    ddt.writer.row += 1
                except Exception as e:
                    logger.exception(e)
                    # 写入失败的结果
                    ddt.writer.write(ddt.writer.row, 7, 'FAIL', 2)
                    ddt.writer.write(ddt.writer.row, 8, e.__str__())
                    # 运行失败后，行数就不是加1了！应该是加剩下没跑的步骤数量
                    ddt.writer.row += len(params) - i
                    # 失败截图
                    self.jie_tu(ddt.web.driver, '失败截图')
                    # 主动让用例失败
                    pytest.fail(e.__str__())

        # 成功截图
        self.jie_tu(ddt.web.driver, '成功截图')
