# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/25 20:14
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：如何获取项目的绝对路径
"""
import os.path
from class16.Logger import logger

# 获取绝对路径（相对当前运行入口的）
ab_path = os.path.abspath('./')
# 处理成项目的绝对路径
ab_path = ab_path[:ab_path.rfind('vip15')+len('vip15')]
print(ab_path)

img_path = ab_path +'\lib\will.png'
print(img_path)
logger.info(img_path)







