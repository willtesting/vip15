# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/22 21:55
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：参数处理
"""

# 截取到最后一个不为空的参数
params = ['', '', '']
idx = -1
# 获取最后一个不为空的下标
for i in range(len(params) - 1, -1, -1):
    if params[i]:
        idx = i
        break

params = params[:idx+1]
print(params)