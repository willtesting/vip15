# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/22 21:38
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：反射
"""
from class11.webkeys import Web

web = Web()

while True:
    func_name = input('请输入关键字：')
    print(func_name)
    # 从一个对象里面，获取一个叫func_name的属性或者函数
    func = getattr(web, func_name)
    # 如果你输入的是openbrowser
    # 现在这个func就等于openbrowser这个函数
    # web.openbrowser()
    func()