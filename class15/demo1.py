# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/22 21:17
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：if语句执行用例
"""
from class11.webkeys import Web

one_case = [['testsuite', 'testcase', 'teststep', '关键字', '输入', '', '', '执行状态', '实际结果'],
            ['登录功能', '', '', '', '', '', '', '', ''],
            ['', '登录失败', '', ' ', '', '', '', '', ''],
            ['', '', '打开浏览器', 'openbrowser', '', '', '', '', ''],
            ['', '', '进入登录页面', 'geturl', 'http://testingedu.com.cn:8000/index.php/Home/user/login.html', '', '', '',
             ''],
            ['', '', '输入用户名', 'input', '//*[@id="username"]', '13800138006', '', '', ''],
            ['', '', '输入密码', 'input', '//*[@id="password"]', '1234561', '', '', ''],
            ['', '', '输入验证码', 'input', '//*[@id="verify_code"]', '11111', '', '', ''],
            ['', '', '点击登录', 'click', '//*[@id="loginform"]/div/div[6]/a', '', '', '', ''],
            ['', '', '等待', 'sleep', '1', '', '', '', ''],
            ['', '', '获取提示', 'gettext', '//*[@id="layui-layer1"]/div[2]', '', '', '', ''], ]


def run_case(line, obj):
    """
    运行的函数
    :param line: 一行用例数据
    :param obj: 关键字对象
    :return:
    """
    # 第一列或者第二列有数据，代表是分组，不用跑
    if len(line[0]) > 0 or len(line[1]) > 0:
        print(line)
        return

    # 获取关键字
    key = line[3]

    if key == "openbrowser":
        obj.openbrowser(line[4], line[5])
    elif key == "geturl":
        obj.geturl(line[4])
    elif key == "input":
        obj.input(line[4], line[5])
    elif key == "click":
        obj.click(line[4])
    elif key == "sleep":
        obj.sleep(line[4])
    elif key == "gettext":
        obj.gettext(line[4])
    else:
        print(f'关键字{key}未实现')


# 创建一个Web对象，用来执行用例
web = Web()
for i in range(len(one_case)):
    line = one_case[i]
    run_case(line, web)
