# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/22 21:33
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：excel用例执行
"""
import traceback

from class11.webkeys import Web
from class15.Excel import Reader


def run_case_new(line, obj):
    """
    反射执行
    :param line: 用例
    :param obj: 关键字对象
    :return:
    """
    # 第一列或者第二列有数据，代表是分组，不用跑
    if len(line[0]) > 0 or len(line[1]) > 0:
        print(line)
        return

    key = line[3]
    try:
        # 反射获取关键字函数
        func = getattr(obj, key)
        # 获取参数
        params = line[4:7]
        idx = -1
        # 获取最后一个不为空的下标
        for i in range(len(params) - 1, -1, -1):
            if params[i]:
                idx = i
                break

        params = params[:idx + 1]
        func(*params)
    except:
        print(f'关键字{key}不存在')
        print(traceback.format_exc())


web = Web()

reader = Reader()
# 打开一个excel
reader.open_excel('./电商项目用例.xlsx')
# 获取所有sheet
sheets = reader.get_sheets()
for sheet in sheets:
    # 设置读取的sheet页面
    reader.set_sheet(sheet)
    # 读取当前sheet的所有行
    lines = reader.readline()
    # print(lines)
    for line in lines:
        run_case_new(line, web)
