# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/13 20:54
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请求体参数的传递方式（主要的两种）
"""
import json

import requests


# 第一种 form表单参数,Content-Type:application/x-www-form-urlencoded ,格式参数（url编码格式）
# 电商登录
url_params = {
    'm': 'Home',
    'c': 'User',
    'a': 'do_login',
    't': '0.3388031582880604',
}
body_params = {
    'username': '13800138006',
    'password': '12345',
    'verify_code': '123456'
}
resp = requests.post('http://testingedu.com.cn:8000/index.php',
                     params=url_params,
                     data=body_params)
# 文本格式
print(resp.text)
# # json格式
# print(resp.json())
# 兼容型写法
try:
    res = json.loads(resp.text)
    print(res)
except:
    print(resp.text)

# 二进制格式
print(resp.content)
# 可以解码
print(resp.content.decode('utf8'))
print(resp.content.decode('unicode-escape'))
# 获取请求
print(resp.request.body)
# 获得结果的编码格式
print(resp.encoding)
# 获取cookie
print(resp.cookies)
# 获取响应头
print(resp.headers)


# # 第二种，json格式参数。Content-Type:application/json。
# # 平台的登录：http://39.108.55.18/mypro/index.html#/login
# body_params = {"username":"Will","pwd":"123456"}
# resp = requests.post('http://39.108.55.18/mypro/api/user/login',
#                      json=body_params)
# print(resp.json())