# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/13 21:26
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：文件的上传和下载
"""
from urllib.parse import unquote

import requests


session = requests.session()

body_params = {"username":"Will","pwd":"123456"}
resp = session.post('http://39.108.55.18/mypro/api/user/login',
                     json=body_params)
print(resp.json())

# 下载
resp = session.get('http://39.108.55.18/mypro/api/res/exportres?id=40')
# 获取下载的文件的二进制编码
img_data = resp.content
print(img_data)
filename = resp.headers.get('Content-Disposition').split('filename="')[1][:-1]
filename = unquote(filename)
print(filename)
# 把二进制写入文件就可以了
with open(filename,mode='wb') as f:
    f.write(img_data)


# 文件上传
# 给一个字典，键是file，值是文件的二进制编码
file_params = {'image':open('mn.jpg',mode='rb')}
resp = requests.post('https://graph.baidu.com/upload',
                     files=file_params)
print(resp.text)






