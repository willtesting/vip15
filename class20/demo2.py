# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/13 20:47
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：requests传递GET方式参数的两种方式
post等方式也是可以在url后面传参的，也是这两种方式
"""
import requests

# 直接带在url后面，问号前面是url，后面是参数
resp = requests.get('https://qifu-api.baidubce.com/ip/geo/v1/district?ip=113.218.168.155')
# 打印结果
print(resp.text)

# 通过params=dict的形式传递
ip_params = {'ip':'113.218.168.155'}
resp = requests.get('https://qifu-api.baidubce.com/ip/geo/v1/district',
                    params=ip_params)
print(resp.text)
# 本质还是带在url后面
print(resp.request.url)






