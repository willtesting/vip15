# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/13 20:41
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：Python如何调用http接口
"""
import requests


# 调接口
resp = requests.get('https://qifu-api.baidubce.com/ip/geo/v1/district?ip=113.218.168.155')
# 打印状态码
print(resp.status_code)
# 打印响应
print(resp.text)
# 打印响应（json解析后的结果）
print(resp.json())
# 获取请求
print(resp.request)