# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/13 21:56
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：会话管理
"""
import requests

# 会话管理
session = requests.session()


# 电商登录
url_params = {
    'm': 'Home',
    'c': 'User',
    'a': 'do_login',
    't': '0.3388031582880604',
}
body_params = {
    'username': '13800138006',
    'password': '123456',
    'verify_code': '123456'
}
resp = session.post('http://testingedu.com.cn:8000/index.php',
                     params=url_params,
                     data=body_params)
# print(resp.json())
# 响应头
print(resp.headers)


# 获取用户信息（一定是先要登录成功的）
resp = session.get('http://testingedu.com.cn:8000/Home/User/info.html')
# 请求头
print(resp.request.headers)
print(session.headers)
print(session.cookies)
# print(resp.text)








