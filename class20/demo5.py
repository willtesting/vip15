# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/13 21:40
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：url格式解码
"""
from urllib.parse import unquote,quote


s = '%E7%89%B9%E6%96%AF%E6%B1%80%E6%8E%A5%E5%8F%A3%E9%A1%B9%E7%9B%AE%E8%87%AA%E5%8A%A8%E5%8C%96%E7%94%A8%E4%BE%8B-40.xlsx'
# 解码
s = unquote(s)
print(s)
# 编码
s = quote(s)
print(s)

