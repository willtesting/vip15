# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/11 20:30
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：多线程实现
"""
import os
import threading
import time

appiumpath = 'D:\software\Appium'
cmd = rf'node "{appiumpath}\resources\app\node_modules\appium\build\lib\main.js"'
print(cmd)


def run_appium_server(cmd):
    """使用一个函数，去实现一件事"""
    # 阻塞
    # Execute the command in a subshell
    # 在你系统的命令行里面执行这个命令
    os.system(cmd)


# 创建一个线程（这个线程完成指定的事情）
# target指定线程需要处理的函数
# args是该函数需要的参数
th = threading.Thread(target=run_appium_server, args=(cmd,))
# 设置为守护进程（主线程执行完成后，子线程就会终止）
# th.daemon = True
# 启动线程
th.start()


time.sleep(5)
print('执行完成')