# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/11 20:26
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
import os

appiumpath = 'D:\software\Appium'

cmd = rf'node "{appiumpath}\resources\app\node_modules\appium\build\lib\main.js" -p 4725'
print(cmd)

# 阻塞
os.system(cmd)

# 这里执行了吗？
print('执行完成')
