# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/11 20:41
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：关闭appium服务
"""
import os
# 复杂场景可以用这个库
# subprocess

port = 4725
# 先获取端口的占用情况
cmd_result =os.popen(f'netstat -aon | findstr {port}').read().split()
print(cmd_result)
if cmd_result:
    pid = cmd_result[-1]
    # res = os.popen(f'taskkill /F /pid {pid}')
    # print(res.read())
else:
    print('端口未占用')










