# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/11 20:11
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：APP关键字封装
"""
import json
import os
import threading
import time

from appium import webdriver

from class16.Logger import logger


class App:
    def __init__(self):
        # 指定变量类型
        self.driver: webdriver.Remote = None
        # appium服务的端口
        self.port = '4723'

    def get_port_pid(self, port):
        # 先获取端口的占用情况
        cmd_result = os.popen(f'netstat -aon | findstr {port}').read().split()
        if cmd_result:
            pid = cmd_result[-1]
            return pid
        else:
            return None

    def runappium(self, port='4723', appiumpath=''):
        """启动appium服务"""
        # 处理默认端口
        if not port:
            port = '4723'

        self.port = port

        # 处理默认路径
        if not appiumpath:
            appiumpath = 'D:\software\Appium'

        cmd = rf'node "{appiumpath}\resources\app\node_modules\appium\build\lib\main.js" -p {port}'

        # 多线程启动appium服务
        def run_appium_server(cmd, port):
            # 先看端口是否占用
            pid = self.get_port_pid(port)
            if pid:
                logger.error(f'端口：{port}已被占用，服务启动失败！')
            else:
                os.system(cmd)

        th = threading.Thread(target=run_appium_server, args=(cmd, port))
        th.start()
        logger.info('appium正在启动....')
        time.sleep(5)

    def stopappium(self):
        pid = self.get_port_pid(self.port)
        if pid:
            res = os.popen(f'taskkill /F /pid {pid}')
            logger.info(res.read())
        else:
            logger.warn('appium服务不存在，关闭失败')

    def runapp(self, conf=''):
        """
        启动APP
        :param conf: APP和设备的相关配置（标准json字符串）
        :return:
        """
        if conf:
            conf = json.loads(conf)
            logger.info(conf)
        else:
            logger.error('没有获得APP的配置')
            return

        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", conf)
        # 添加隐式等待
        self.driver.implicitly_wait(10)

    def find_ele(self, locator=''):
        """
        统一三种定位方式
        :param locator:
        :return:
        """
        if locator.__contains__(':id/'):
            # id定位
            ele = self.driver.find_element('id', locator)
        elif locator.startswith('/') or locator.startswith('('):
            # xpath定位
            ele = self.driver.find_element('xpath', locator)
        else:
            ele = self.driver.find_element('accessibility id', locator)

        return ele

    def click(self, lo=''):
        ele = self.find_ele(lo)
        ele.click()

    def input(self, lo='', value='',enter=''):
        ele = self.find_ele(lo)
        ele.click()
        ele.send_keys(value)
        if enter:
            self.driver.keyevent(66)

    def sleep(self, t='1'):
        try:
            t = float(t)
        except:
            t = 1

        time.sleep(t)

    def quit(self):
        self.driver.quit()

if __name__ == '__main__':
    app = App()
    app.runappium('4725')
    time.sleep(5)
    app.stopappium()
