# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/11 21:20
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
import json

conf ="""
        {
  "platformName": "Android",
  "deviceName": "emulator-5554",
  "appPackage": "com.tencent.mobileqq",
  "appActivity": ".activity.SplashActivity",
  "noReset": "true"
}
        """
conf = json.loads(conf)
print(conf)