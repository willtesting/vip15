# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/15 20:19
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：运行入口
"""
import os
import pytest

os.system('rd /s/q report')
os.system('rd /s/q result')

# 在pytest运行的同时，先生成执行结果
pytest.main(['-s', '--alluredir', './result'])
# # 发布报告
# os.system('allure serve result')

# 生成可发布的报告
os.system('allure generate result -o report --clean')
