# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/18 22:03
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：全局参数管理
"""
import pytest

from class13.pages.webkeys import BasePage


# # 全局变量：实现所有测试模块共享数据
# bs = BasePage()
# bs.openbrowser('ed')
# driver = bs.driver

@pytest.fixture(scope='session')
def get_driver():
    """
    The scope for which this fixture is shared; one of ``"function"``
        (default), ``"class"``, ``"module"``, ``"package"`` or ``"session"``.
    :return:
    """
    bs = BasePage()
    bs.openbrowser('ed')
    driver = bs.driver
    yield driver
