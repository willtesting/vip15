# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/18 20:13
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：个人中心
"""
import allure
import pytest


class TestUser:
    """个人信息"""

    @pytest.mark.parametrize('argname', [[1, 1], [2, 2], [3, 3]])
    def test_avatar(self, argname):
        """
        头像上传
        参数化示例
        :param argname: 接收数据的参数名，装饰器和用例的参数名必须一模一样
        :return:
        """
        # 运行原理，就把后面列表的元素迭代的执行测试用例
        # 每一次迭代执行，把列表的元素值，传给参数
        print(argname)
        allure.dynamic.title('头像上传')
        print('头像上传待实现')

    def test_userinfo(self):
        """修改个人信息"""
        allure.dynamic.title('修改个人信息')
        print('修改个人信息待实现')


class TestAddress:
    """地址管理"""

    def test_add_address(self):
        """新增收货地址"""
        allure.dynamic.title('新增地址')
        print('新增地址待实现')

    def test_del_address(self):
        """删除收货地址"""
        allure.dynamic.title('删除收货地址')
        print('删除收货地址待实现')
