# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/18 20:07
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：测试模块-登录+注册
"""
import allure
import pytest
from class13.pages.webkeys import BasePage
# from class13.conftest import driver


@allure.feature('登录+注册')
class TestLogin:
    """登录的Page类"""

    def setup_class(self):
        """打开浏览器"""
        self.web = BasePage()

    @pytest.mark.parametrize(
        'params', [['手机号登录成功', '13800138006', '123456'],
                   # ['手机号登录失败', '13800138006', '1234567'],
                   # ['邮箱号登录成功', '123@qq.com', '123456'],
                   ['邮箱号登录失败', '123@qq.com', '1234567'], ])
    @allure.story('登录')
    def test_login(self, params,get_driver):
        """一个功能点，即是一个用例"""
        allure.dynamic.title(params[0])
        self.web.driver = get_driver
        # 登录
        self.web.geturl('http://39.108.55.18:8000/Home/user/login.html')

        # 判断页面的登录态，如果已登录就注销
        title = self.web.gettitle()
        if not title.startswith('登录'):
            self.web.click('//a[text()="安全退出"]')
            # 登录
            self.web.geturl('http://39.108.55.18:8000/Home/user/login.html')

        with allure.step('输入用户名'):
            self.web.input('//*[@id="username"]', params[1])

        self.web.input('//*[@id="password"]', params[2])
        self.web.input('//*[@id="verify_code"]', '{verify}')
        self.web.click('//*[@id="loginform"]/div/div[6]/a')

        # 以业务数据进行校验判断
        if params[0].__contains__('失败'):
            msg = self.web.gettext('//*[@id="layui-layer1"]/div[2]')
            assert msg.__contains__('密码错误')
        else:
            # 判断页面的登录态，如果已登录就注销
            title = self.web.gettitle()
            if title.startswith('登录'):
                pytest.fail('登录失败')

    @allure.story('联合登录')
    def test_union_login(self):
        """联合登录"""
        allure.dynamic.title('联合登录')
        print('待实现')

    # @pytest.mark.parametrize('p', ['淘宝网', '京东', '唯品会', '苏宁易购', '百度', '1号店'])
    # @allure.story('链接测试')
    # def test_link(self, p):
    #     """链接测试"""
    #     allure.dynamic.title(p)
    #     # 登录
    #     self.web.geturl('http://39.108.55.18:8000/Home/user/login.html')
    #
    #     # 判断页面的登录态，如果已登录就注销
    #     title = self.web.gettitle()
    #     if not title.startswith('登录'):
    #         self.web.click('//a[text()="安全退出"]')
    #         # 登录
    #         self.web.geturl('http://39.108.55.18:8000/Home/user/login.html')
    #
    #     h1 = self.web.driver.window_handles
    #     self.web.click(f'//a[text()="{p}"]')
    #     # 判断如果打开了新窗口，就切换到新窗口
    #     h2 = self.web.driver.window_handles
    #     if len(h1) < len(h2):
    #         self.web.switchwin('-1')
    #
    #
    #     # 断言title
    #     title = self.web.gettitle()
    #     assert title == '' or title.__contains__(p[0:2])

    def teardown_method(self):
        # 因为每一组用例跑完都要加截图
        self.web.sleep('0.5')
        allure.attach(self.web.driver.get_screenshot_as_png(), '用例结果截图', allure.attachment_type.PNG)


@allure.feature('登录+注册')
class TestRegister:
    """注册页面-Page类"""
