# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/18 22:25
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：迭代器和生成器
"""

myl = [1, 2, 3, 4, 5, 6]
# 获取迭代器
it = myl.__iter__()
while True:
    # 下一个：迭代获取列表的每一个元素（遍历）
    try:
        print(it.__next__())
    except:
        break


# 生成器函数
def my_generator():
    print('开始生成')
    yield 1
    print('第二次生成')
    yield 2
    print('第三次生成')
    yield 3


# 通过调用生成器函数，你得到的是一个生成器
gen = my_generator()
while True:
    try:
        gen.__next__()
    except:
        break


# 生成器函数
def my_generator1():
    for i in range(1000):
        yield i

g = my_generator1()
s = g.__next__()
print(s)
s = next(g)
print(s)
