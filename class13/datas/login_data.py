# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/18 21:46
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：登录相关数据
"""


class LoginData:
    """登录相关数据"""
    url = 'http://39.108.55.18:8000/Home/user/login.html'
    exit_lo = '//a[text()="安全退出"]'
    u_lo = '//*[@id="username"]'
    p_lo = '//*[@id="password"]'
    v_lo = '//*[@id="verify_code"]'
    login_btn = '//*[@id="loginform"]/div/div[6]/a'
    msg_lo = '//*[@id="layui-layer1"]/div[2]'

    case_data = [['手机号登录成功', '13800138006', '123456', '我的账户'],
                 # ['手机号登录失败', '13800138006', '1234567', '密码错误'],
                 # ['邮箱号登录成功', '123@qq.com', '123456', '我的账户'],
                 ['邮箱号登录失败', '123@qq.com', '1234567', '密码错误'], ]
