# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/13 20:08
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：Web自动化关键字
"""
import os
import re
import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from class10.chaojiying import Chaojiying_Client
from class11.relation_decs import relations


class BasePage:
    """基本操作：基础功能操作"""
    def __init__(self,driver=None):
        """初始化"""
        # 全局的浏览器对象：指定类型，方便后面点出方法
        self.driver: webdriver.Chrome = driver
        # 管理关联数据的字典
        self.relation_dict = {}

    def openbrowser(self, browser: str = 'gc', im: str = '10'):
        """
        打开浏览器
        功能1：选择打开什么浏览器
        功能2：默认添加隐式等待
        :param browser: 浏览器类型（gc(default),ff,ed...）
        :param im: 隐式等待时间（默认10s）
        :return:
        """
        if browser == 'ed':
            option = webdriver.EdgeOptions()
            # 不自动关闭浏览器的配置
            option.add_experimental_option("detach", True)
            # 在打开浏览器之前，去掉自动化标识
            option.add_experimental_option('excludeSwitches', ['enable-automation'])
            option.add_argument('--disable-blink-features=AutomationControlled')

            # 关掉密码弹窗
            prefs = {}
            prefs['credentials_enable_service'] = False
            prefs['profile.password_manager_enabled'] = False
            option.add_experimental_option('prefs', prefs)
            self.driver = webdriver.Edge(options=option)
        elif browser == 'ff':
            self.driver = webdriver.Firefox()
        else:
            option = webdriver.ChromeOptions()
            # 不自动关闭浏览器的配置
            option.add_experimental_option("detach", True)
            # 在打开浏览器之前，去掉自动化标识
            option.add_experimental_option('excludeSwitches', ['enable-automation'])
            option.add_argument('--disable-blink-features=AutomationControlled')

            # 关掉密码弹窗
            prefs = {}
            prefs['credentials_enable_service'] = False
            prefs['profile.password_manager_enabled'] = False
            option.add_experimental_option('prefs', prefs)
            # 默认是打开谷歌浏览器
            self.driver = webdriver.Chrome(options=option)

        # 处理隐式等待
        try:
            im = float(im)
        except:
            im = 10
        self.driver.implicitly_wait(im)

        # 默认最大化
        self.driver.maximize_window()

    def geturl(self, url: str = ''):
        """
        打开网站
        :param url: 需要打开网站地址
        :return:
        """
        # 兼容不标准的url地址
        if not url.startswith('http'):
            url = 'http://' + url

        self.driver.get(url)

    def find_ele(self, lo: str = ''):
        """
        找元素
        兼容id，xpath定位
        :param lo:
        :return:
        """
        if lo.startswith('/') or lo.startswith('('):
            # xpath定位
            ele = self.driver.find_element('xpath', lo)
        else:
            ele = self.driver.find_element('id', lo)

        return ele

    @relations
    def input(self, lo: str = '', value: str = ''):
        """
        输入或者文件上传
        文件上传需要兼容：
        1. 相对路径
        2. 批量上传
        :param lo: 元素定位
        :param value: 输入的值
        :return:
        """
        ele = self.find_ele(lo)
        # 兼容批量
        if value.startswith('[') and value.endswith(']'):
            # 如果传进来的数据是一个列表格式字符串，就认为是匹配上传
            # 兼容写单斜杠路径
            value = value.replace('\\', '\\\\')
            # 这里兼容写双斜杠路径
            value = value.replace('\\\\\\\\', '\\\\')
            value = eval(value)
            # 处理批量的相对路径
            for i in range(len(value)):
                if not value[i].__contains__(':'):
                    # 如果不是绝对路径
                    value[i] = os.path.abspath('../../') + '\\lib\\' + value[i]

            value = '\n'.join(value)
        elif value.endswith('.png'):
            # 处理单个文件的相对路径
            if not value.__contains__(':'):
                # 如果不是绝对路径
                value = os.path.abspath('../../') + '\\lib\\' + value

        ele.send_keys(value)

    def click(self, lo: str = ''):
        """
        找到并点击元素
        :param lo: 定位表达式支持id，xpath
        :return:
        """
        ele = self.find_ele(lo)
        ele.click()

    def gettext(self, lo: str = '', reg: str = ''):
        """
        获取元素的文本
        如果传了reg正则表达式，就返回正则表达式处理后的结果
        :param lo: 定位表达式支持id，xpath
        :return:
        """
        ele = self.find_ele(lo)
        text = ele.text
        # 处理正则结果
        if reg:
            text = re.findall(reg, text)
            if text:
                text = text[0]

        self.relation_dict['text'] = text
        return text

    @relations
    def gettitle(self):
        self.sleep('1')
        title = self.driver.title
        self.relation_dict['title'] = title
        return title

    @relations
    def getverify(self, lo: str = ''):
        """
        获取图文验证码
        :param lo: 验证码图片定位器
        :return:
        """
        # 获取验证码元素
        ele = self.find_ele(lo)
        # 针对元素截图
        ele.screenshot('verify.png')
        # 调超级鹰破解验证码
        chaojiying = Chaojiying_Client('wuqingfqng', '6e8ebd2e301f3d5331e1e230ff3f3ca5', '96001')
        ver = chaojiying.PostPic('verify.png', 1902).get('pic_str')
        # 添加一个叫verify的关联键值对
        self.relation_dict['verify'] = ver
        return ver

    @relations
    def saveparams(self, paramname: str = '', value: str = ''):
        """
        把数据保存为某个参数名字
        :param paramname: 参数名字，用来调用这个数据
        :param value: 保存的值
        :return:
        """
        self.relation_dict[paramname] = value

    def move_and_click(self, lo1: str = '', lo2: str = ''):
        """
        鼠标移动到元素1，点击元素2
        :param lo1: 元素1
        :param lo2: 元素2
        :return:
        """
        action = ActionChains(self.driver)
        # 鼠标移动到元素
        action.move_to_element(self.find_ele(lo1))
        # 使所有鼠标操作生效
        action.perform()
        self.click(lo2)

    def intoiframe(self, lo: str = ''):
        """
        找到并点击元素
        :param lo: 定位表达式支持id，xpath
        :return:
        """
        ele = self.find_ele(lo)
        self.driver.switch_to.frame(ele)

    def waittext(self, lo: str = '', text: str = ''):
        """
        显示等待，等待元素出现指定文本
        :param lo: 元素
        :param text: 指定文本
        :return:
        """
        # 显示等待
        # 处理定位
        if lo.startswith('/') or lo.startswith('('):
            # xpath定位
            lo = ('xpath', lo)
        else:
            lo = ('id', lo)

        # 每隔0.5s判断一次，直到10s，就报错
        WebDriverWait(self.driver, 10, 0.5).until(
            # 判断条件，是//div[@class="info"]元素的文本内容包含：已上传1份
            EC.text_to_be_present_in_element(lo, text)
        )

    def sleep(self, t: str = '1'):
        """
        固定等待
        :param t: 时间
        :return:
        """
        try:
            t = float(t)
        except:
            t = 1

        time.sleep(t)

    def select(self, lo: str = '', index_text: str = '1'):
        """
        select元素选择
        :param lo: select元素定位
        :param index_text: 选择下标，或者可见文本
        :return:
        """
        ele = self.find_ele(lo)
        select = Select(ele)
        # 如果可以转成整数，我们就用下标来选择
        try:
            index_text = int(index_text)
            select.select_by_index(index_text)
        except:
            select.select_by_visible_text(index_text)

        self.sleep('0.5')

    def switchwin(self, index_title: str = '0'):
        """
        窗口切换
        :param index_title: 切换的下标或者title
        :return:
        """
        handles = self.driver.window_handles
        try:
            # 按下标切换
            index_title = int(index_title)
            self.driver.switch_to.window(handles[index_title])
        except:
            # 按title切换
            # 遍历所有的窗口id
            for h in handles:
                # 每切换一个窗口，就获取title
                self.driver.switch_to.window(h)
                t = self.driver.title
                # 如果当前窗口的标题包含需要的标题，就不再切换
                if t.__contains__(index_title):
                    break
