# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/18 21:35
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：登录页面的封装
"""
from class13.datas.login_data import LoginData
from class13.pages.webkeys import BasePage


class LoginPage(BasePage):
    """登录页面的操作层：业务相关操作"""

    def __init__(self,driver=None):
        BasePage.__init__(self,driver)

    def login(self, u, p):
        """登录页面的操作"""
        self.geturl(LoginData.url)
        # 判断页面的登录态，如果已登录就注销
        title = self.gettitle()
        if not title.startswith('登录'):
            self.click(LoginData.exit_lo)
            # 登录
            self.geturl('http://39.108.55.18:8000/Home/user/login.html')

        self.input(LoginData.u_lo, u)
        self.input(LoginData.p_lo, p)
        self.input(LoginData.v_lo, '{verify}')
        self.click(LoginData.login_btn)

        title = self.gettitle()
        if title.startswith('登录'):
            # 登录失败，就返回提示信息
            return self.gettext(LoginData.msg_lo)
        else:
            # 登录成功就返回title
            return title

    def link(self, p):
        """链接测试的功能操作"""
        # 登录
        self.geturl('http://39.108.55.18:8000/Home/user/login.html')

        # 判断页面的登录态，如果已登录就注销
        title = self.gettitle()
        if not title.startswith('登录'):
            self.click('//a[text()="安全退出"]')
            # 登录
            self.geturl('http://39.108.55.18:8000/Home/user/login.html')

        h1 = self.driver.window_handles
        self.click(f'//a[text()="{p}"]')
        # 判断如果打开了新窗口，就切换到新窗口
        h2 = self.driver.window_handles
        if len(h1) < len(h2):
            self.switchwin('-1')

        # 返回新打开链接的title
        title = self.gettitle()
        return title
