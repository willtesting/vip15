# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/18 21:53
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：登录的业务层
"""
import allure
import pytest

from class13.datas.login_data import LoginData
from class13.pages.loginpage import LoginPage


# from class13.conftest import driver


@allure.feature('new登录')
class TestNewLogin:
    def setup_class(self):
        self.login = LoginPage()

    @pytest.mark.parametrize('params', LoginData.case_data)
    @allure.story('新登录')
    def test_login(self, params, get_driver):
        self.login.driver = get_driver
        allure.dynamic.title(params[0])
        res = self.login.login(params[1], params[2])
        assert res.__contains__(params[3])
