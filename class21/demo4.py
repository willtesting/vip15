# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/16 21:47
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：登录接口解耦合
"""
from class21.interkeys import Inter

inter = Inter()
inter.seturl('http://testingedu.com.cn:8081/inter/HTTP')

inter.reauth()
# 登录
inter.post('login', 'username=Will&password=123456')

inter.reauth()
# 登录
inter.post('login', 'username=Will&password=1234567')

inter.reauth()
# 登录
inter.post('login', 'username=Will')

# 注销
inter.post('logout')
