# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/16 21:11
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：关键字执行
"""
from class21.interkeys import Inter

inter = Inter()
inter.seturl('http://testingedu.com.cn:8081/inter/HTTP')

# 获取token
inter.post('auth')
inter.addheader('token', '{token}')

inter.post('register','username=Will14&pwd=123456&nickname=测试账号&describe=')

# 登录
inter.post('login','username=Will14&password=123456')

inter.savejson('userid,msg', '')

# 获取用户信息
inter.post('getUserInfo',
           'id={userid}')
# 注销
inter.post('logout')
