# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/16 20:50
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：url格式参数转字典
"""
params = 'username=Will&password=1231231&1323123'
# 先切割键值对
key_values = params.split('&')
# 再提取键和值
params_dict = {}
for key_value in key_values:
    key_value = key_value.split('=')
    if len(key_value) == 1:
        params_dict[key_value[0]] = None
    else:
        params_dict[key_value[0]] = key_value[1]

print(params_dict)