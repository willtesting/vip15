# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/16 20:19
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：特斯汀接口项目可行性分析
分析的接口包括：注册->登录->用户信息->注销
"""
import json
import requests


# 使用会话管理，合理简化一些通用的数据
session = requests.session()

# 获取token
resp = session.post('http://testingedu.com.cn/inter/HTTP/auth')
print(resp.text)
try:
    resp_dict = json.loads(resp.text)
    session.headers['token'] = resp_dict.get('token')
except:
    print('授权失败')
    print(resp.text)

# 接口信息提取
url = 'http://testingedu.com.cn:8081/inter/HTTP/register'
params = {
    'username': 'Will13',
    'pwd': '123456',
    'nickname': '测试账号',
    "describe": None
}
resp = session.post(url, data=params)
print(resp.text)

# 登录
resp = session.post('http://testingedu.com.cn:8081/inter/HTTP/login',
                     data={'username':'Will11','password':'123456'})
print(resp.text)
try:
    resp_dict = json.loads(resp.text)
    userid = resp_dict.get('userid')
except:
    print('登录失败')
    print(resp.text)
    userid = None

# 获取用户信息
resp = session.post('http://testingedu.com.cn:8081/inter/HTTP/getUserInfo',
                     data={'id':userid})
print(resp.text)

# 注销
resp = session.post('http://testingedu.com.cn:8081/inter/HTTP/logout')
print(resp.text)
