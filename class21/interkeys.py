# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/16 20:44
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：接口关键字封装
"""
import json

import requests

from class11.relation_decs import relations
from class16.Logger import logger


class Inter:
    def __init__(self):
        # 默认还是使用会话管理
        self.session = requests.session()
        # 保存上一步的结果
        self.resp_data = None
        # 关联字典
        self.relation_dict = {}
        # url前缀
        self.url = ''

    def seturl(self, url):
        """
        设置url通用前缀
        :param url: url通用前缀
        """
        self.url = url

    def _get_url_params(self, params):
        """url格式参数转字典"""
        params_dict = {}
        if params:
            # 先切割键值对
            key_values = params.split('&')
            for key_value in key_values:
                key_value = key_value.split('=')
                if len(key_value) == 1:
                    params_dict[key_value[0]] = None
                else:
                    params_dict[key_value[0]] = key_value[1]
            return params_dict
        else:
            return None

    @relations
    def post(self, url: str, params=''):
        """
        发送post请求
        :param url: 接口地址
        :param params: form data参数（备注：json格式可以考虑修改封装方式）
        :return:
        """
        # url处理
        # 如果是标准url，就不拼前缀
        if not url.startswith('http'):
            url = self.url + '/' + url

        # 参数处理
        # # 情况1：如果统一传json字符串，处理方式
        # params = json.loads(params)
        # 情况2：如果统一传url格式字符串
        params = self._get_url_params(params)
        logger.debug(f'请求参数：{params}')
        resp = self.session.post(url, data=params)
        # 结果处理：比如公司返回大部分是通用的json，就进行json解析处理
        # 也有可能，我公司返回的就是xml或者html
        try:
            self.resp_data = json.loads(resp.text)
        except:
            logger.warn('json解析失败')
            # 解析失败，就直接用文本
            self.resp_data = resp.text

        logger.debug(f'请求结果：{self.resp_data}')

    def reauth(self):
        """重新授权"""
        # 先注销
        self.post(self.url + '/logout')
        # 授权
        self.post(self.url + '/auth')
        self.addheader('token', '{token}')

    @relations
    def addheader(self, key, value):
        """
        请求头添加键值对
        :param key: 键
        :param value: 值
        :return:
        """
        self.session.headers[key] = value
        logger.debug(f'请求头：{self.session.headers}')

    @relations
    def savejson(self, keys='', param_names=''):
        """
        把上一步请求的结果，保存为关联的键值对
        :param keys: 键的列表，逗号分割
        :param param_names: 对应参数名字，逗号分割，如果没有参数名，就用默认键名
        """
        if keys:
            keys = keys.split(',')
            if param_names:
                param_names = param_names.split(',')
                for i in range(len(keys)):
                    self.relation_dict[param_names[i]] = self.resp_data.get(keys[i])
            else:
                for key in keys:
                    self.relation_dict[key] = self.resp_data.get(key)

        logger.debug(f'关联字典：{self.relation_dict}')
