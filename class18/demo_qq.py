# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/9 21:42
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：请输入模块功能描述
"""
# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python
import time
from appium import webdriver

caps = {}
caps["platformName"] = "Android"
caps["deviceName"] = "emulator-5554"
caps["appPackage"] = "com.tencent.mobileqq"
caps["appActivity"] = ".activity.SplashActivity"
caps["noReset"] = "true"
# appium,uiautomator1,uiautomator2
caps["automationname"] = "uiautomator1"
# 保证webview也可以识别
caps["ensureWebviewsHavePages"] = True

driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
# 添加隐式等待
driver.implicitly_wait(10)

el1 = driver.find_element('accessibility id',"密码 安全")
el1.send_keys("xiaobao168")
el2 = driver.find_element('id',"com.tencent.mobileqq:id/qqp")
el2.click()
el3 = driver.find_element('accessibility id',"登 录")
el3.click()

# 添加固定等待
time.sleep(8)

el4 = driver.find_element('accessibility id',"帐户及设置")
el4.click()
el5 = driver.find_element('accessibility id',"设置")
el5.click()
el6 = driver.find_element('accessibility id',"帐号管理")
el6.click()
el7 = driver.find_element('accessibility id',"退出")
el7.click()
els4 = driver.find_elements('xpath',"//*[@text=\"退出登录\"]")
els4[0].click()
el8 = driver.find_element('id',"com.tencent.mobileqq:id/dialogRightBtn")
el8.click()

# 添加固定等待
time.sleep(2)
driver.quit()