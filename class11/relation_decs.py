# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/13 22:02
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：关联装饰器
"""


def relations(func):
    def wrapper(*args, **kwargs):
        # 在函数执行前，对参数处理
        args = list(args)
        # 第一个参数一定是self
        self = args[0]
        # 对self以外的每一个参数都进行关联操作
        for i in range(1, len(args)):
            # 兼容接口里面上一步请求的结果直接关联
            if type(self.__dict__.get('resp_data')) == dict:
                for key in self.resp_data.keys():
                    k = '{' + key + '}'
                    args[i] = str(args[i]).replace(k, str(self.resp_data.get(key)))

            for key in self.relation_dict.keys():
                k = '{' + key + '}'
                args[i] = str(args[i]).replace(k, str(self.relation_dict.get(key)))

        res = func(*args, **kwargs)
        return res

    return wrapper
