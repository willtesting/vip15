# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/13 20:38
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：测试代码
"""
import json

# # 批量上传
# import os
#
# s = r"['C:\\Users\\Amy\\Desktop\\will.png','C:\\Users\\Amy\\Desktop\\will.png']"
# s = s.replace('\\','\\\\')
# s = s.replace('\\\\\\\\','\\\\')
# print(s)
# print(eval(s))
#
# # 获取绝对路径
# p = os.path.abspath('../')
# print(p)

# 这个是关联字典里面的数据
r_d = {'text': '田小姐', 'orderid': 'dfdsa23434', 'ds': 5}
# 参数：我规定这个{text}在运行的时候需要替换为关联字典里面text键的值（田小姐）
p = 'nickname={text}！{orderid}'
# 逆向思维，我不管这个字符串里有什么，我都尝试替换：
# 把r_d里面所有键都用{key}，然后尝试替换为键的值
for key in r_d.keys():
    k = '{' + key + '}'
    p = p.replace(k, str(r_d.get(key)))

print(p)
