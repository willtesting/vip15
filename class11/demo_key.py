# -*- coding: utf-8 -*-
"""
@Time ： 2023/9/13 20:17
@Auth ： Mr. William 1052949192
@Company ：特斯汀学院 @testingedu.com.cn
@Function ：关键字驱动用例
"""
from class11.webkeys import Web

# 创建关键字对象
web = Web()
web.openbrowser('ed', '10')

# 登录
web.geturl('http://39.108.55.18:8000/Home/user/login.html')
web.input('//*[@id="username"]', '13800138006')
web.input('//*[@id="password"]', '123456')
web.getverify('//*[@id="verify_code_img"]')
web.input('//*[@id="verify_code"]', '{verify}')
web.sleep('3')
web.click('//*[@id="loginform"]/div/div[6]/a')
web.gettext('//*[@class="red userinfo"]')

# 演示关联
print(web.relation_dict)
web.saveparams('nickname', 'aa{text}bb')
print(web.relation_dict)

# 点击个人中心
web.move_and_click('//span[text()="账户设置"]', '//a[text()="个人信息"]')
web.click('//*[@id="preview"]')
web.intoiframe('//*[contains(@id,"layui-layer-iframe")]')
# 上传文件
# 批量写法
# web.input('//*[@id="filePicker"]//input', '["will.png","small.png"]')
# 单个文件上传
web.input('//*[@id="filePicker"]//input', "will.png")
web.waittext('//div[@class="info"]', '已上传')
web.click('//*[@class="saveBtn"]')
web.sleep('2')

# 点击按钮的跳转，可以改成直接访问地址，这样对前置的依赖就少了
web.geturl('http://39.108.55.18:8000/Home/User/address_list.html')
web.click('//span[text()="增加新地址"]')

# 输入信息
web.input('//input[@name="consignee"]', '老will')
web.input('//input[@name="mobile"]', '18888888888')
# select标签选择
# 把元素转成select对象
web.select('//*[@id="province"]', '辽宁省')
web.select('//*[@id="city"]', '1')
web.select('//*[@id="district"]', '2')
web.select('//*[@id="twon"]', '1')
web.input('//input[@name="address"]', 'dsfkjdsalkjf')
web.click('//*[@id="address_submit"]')
web.sleep('2')

# 删除收货地址
web.click('//span[text()="老will"]/../..//a[text()="删除"]')
web.sleep('2')

# 搜索
web.input('//*[@id="q"]', '手机')
web.click('//*[@id="sourch_form"]/a')

# 加购物车
web.click('//a[contains(text(),"迷你手机456")]/../..//a[text()="加入购物车"]')
web.click('//span[@class="layui-layer-setwin"]')
web.click('//span[text()="我的购物车"]')
web.click('//a[text()="去结算"]')
web.click('//button[@class="checkout-submit"]')
web.gettext('//p[@class="succ-p"]', '\d{18}')
web.saveparams('orderid', '{text}')

# 订单号，订单查询
# 点击我的订单是OK的，结合我们之前的运行情况，发现也确实打开了新的我的订单页面
web.click('//a[text()="我的订单"]')
web.switchwin('我的订单')
# 输入订单，搜索
web.input('//*[@id="search_key"]', '{orderid}')
web.click('//*[@id="search_order"]/input[2]')
